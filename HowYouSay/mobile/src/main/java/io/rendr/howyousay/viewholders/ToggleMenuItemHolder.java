package io.rendr.howyousay.viewholders;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import io.rendr.howyousay.R;
import io.rendr.howyousay.core.models.ToggleMenuItem;

/**
 * Created by ben on 12/28/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class ToggleMenuItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView mTitleTextView;

    public CheckBox mCheckBox;
    public ToggleMenuItem mMenuItem;


    public ToggleMenuItemHolder(View itemView) {
        super(itemView);
        mTitleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
        mCheckBox = (CheckBox)itemView.findViewById(R.id.checkBox);
        mCheckBox.setClickable(false);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mCheckBox.setChecked(!mCheckBox.isChecked());
        mMenuItem.setSelected(mCheckBox.isChecked());
    }
}
