package io.rendr.howyousay.viewholders;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import io.rendr.howyousay.R;

/**
 * Created by ben on 11/17/15.
 */
public class MenuItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final Activity mActivity;
    public TextView mTitleTextView;

    public Intent Intent;

    public MenuItemHolder(Activity activity, View itemView) {
        super(itemView);
        mActivity = activity;
        mTitleTextView = (TextView) itemView.findViewById(R.id.titleTextView);
        itemView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (Intent != null) {
            mActivity.startActivity(Intent);
        }
    }
}
