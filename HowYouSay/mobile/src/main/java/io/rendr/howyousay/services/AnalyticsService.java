package io.rendr.howyousay.services;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import io.rendr.howyousay.BuildConfig;
import io.rendr.howyousay.HowYouSayApplication;
import io.rendr.howyousay.core.database.PhraseBaseHelper;

/**
 * Created by ben on 1/15/16.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean(scope = EBean.Scope.Singleton)
public class AnalyticsService {

    @RootContext
    public Context mContext;
    private Tracker mTracker;

    @AfterInject
    public void init() {
        HowYouSayApplication application = (HowYouSayApplication) mContext;
        mTracker = application.getDefaultTracker();
    }


    public void trackScreen(String screenName)
    {
        Log.i("Analytics", "Setting screen name: " + screenName);
        mTracker.setScreenName(screenName);
        if (!BuildConfig.DEBUG) {
            mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        }

    }
}
