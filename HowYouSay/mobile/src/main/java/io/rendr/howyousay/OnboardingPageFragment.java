package io.rendr.howyousay;


import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OnboardingPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@EFragment(R.layout.fragment_onboarding_2)
public class OnboardingPageFragment extends android.support.v4.app.Fragment {

    @ViewById
    VideoView videoView;

    public OnboardingPageFragment() {
        // Required empty public constructor
    }

    @AfterViews
    void initVideo() {
        String path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_1;
        videoView.setVideoURI(Uri.parse(path));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView.start();
    }

    @Click(R.id.startButton)
    void finish(){
        ((SplashActivity)getActivity()).finishOnboarding();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment OnboardingPageFragment.
     */

    public static OnboardingPageFragment newInstance() {
        OnboardingPageFragment fragment = new OnboardingPageFragment_();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


}
