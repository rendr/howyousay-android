package io.rendr.howyousay.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import io.rendr.howyousay.PhraseListFragment;

/**
 * Created by ben on 11/17/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class MainViewPagerAdapter extends FragmentPagerAdapter {
    private final ArrayList<PhraseListFragment> mFrags;

    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mFrags = new ArrayList<PhraseListFragment>();
        mFrags.add(PhraseListFragment.newInstance(false));
        mFrags.add(PhraseListFragment.newInstance(true));

    }

    public void setSearchCriteria(String searchCriteria){
        for (PhraseListFragment  frag: mFrags) {
            frag.setSearchCriteria(searchCriteria);
        }

    }

    @Override
    public Fragment getItem(int position) {
        return mFrags.get(position);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return "Full List";
        }else{
            return "Bookmarked";
        }
    }
}
