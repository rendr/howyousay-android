package io.rendr.howyousay;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.io.Console;

import io.rendr.howyousay.adapters.OnboardingViewPagerAdapter;
import io.rendr.howyousay.core.services.AudioFileCleanUpService;
import io.rendr.howyousay.core.services.VocabSeedsAvailableLanguagesService;
import io.rendr.howyousay.services.AnalyticsService;
import io.rendr.howyousay.services.PermissionsService;
import main.java.com.mindscapehq.android.raygun4android.RaygunClient;
import main.java.com.mindscapehq.android.raygun4android.messages.RaygunUserInfo;
@EActivity(R.layout.activity_splash)
@OptionsMenu(R.menu.menu_splash)
public class SplashActivity extends AppCompatActivity {


    public static final String SCREEN_NAME = "Onboarding";

    static String PREF_FIRST_RUN = "prefFirstRun";

    static String FIRST_RUN_COMPLETE_KEY = "firstRunComplete";

    @Bean
    public VocabSeedsAvailableLanguagesService AvailableLanguagesService;

    @Bean
    public AnalyticsService mAnalyticsService;

    @ViewById
    ViewPager pager;

    @ViewById
    Toolbar toolbar;

    private OnboardingViewPagerAdapter mPagerAdapter;
    private SharedPreferences mSettings;

    @AfterViews
    void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
    }

    @AfterViews
    void initViewPager() {
        mPagerAdapter = new OnboardingViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(mPagerAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            OnboardingAddPhrases addPhrasesPage = (OnboardingAddPhrases) mPagerAdapter.getItem(pager.getCurrentItem());
            addPhrasesPage.addPhrases();
        } else {
            PermissionsService.displaySeedPermissionsNeededDialog(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RaygunClient.Init(getApplicationContext());
        RaygunClient.AttachExceptionHandler();


    }

    @Override
    protected void onStart() {
        mSettings = getSharedPreferences(PREF_FIRST_RUN, 0);
        super.onStart();
        boolean firstRunComplete = mSettings.getBoolean(FIRST_RUN_COMPLETE_KEY, false);
        if(firstRunComplete){
            finishOnboarding();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        mAnalyticsService.trackScreen(SCREEN_NAME);
    }

    @OptionsItem(R.id.action_next)
    public void next() {
        if(pager.getCurrentItem() == 0){
            pager.setCurrentItem(1, true);
        }else {
            finishOnboarding();
        }
    }


    protected void finishOnboarding() {
        SharedPreferences.Editor prefEditor = mSettings.edit();
        prefEditor.putBoolean(FIRST_RUN_COMPLETE_KEY, true);
        prefEditor.commit();
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }


}
