package io.rendr.howyousay.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import io.rendr.howyousay.R;
import io.rendr.howyousay.core.models.DrawerMenuItem;
import io.rendr.howyousay.viewholders.MenuItemHolder;

/**
 * Created by ben on 11/17/15.
 */
public class MainMenuAdapter extends RecyclerView.Adapter<MenuItemHolder> {

    private final ArrayList<DrawerMenuItem> mItems;
    private final Activity mActivity;


    public MainMenuAdapter(Activity activity, ArrayList<DrawerMenuItem> items) {
        mItems = items;
        mActivity = activity;

    }

    @Override
    public MenuItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from( mActivity);
        View view = layoutInflater.inflate(R.layout.main_menu_item, viewGroup, false);
        return new MenuItemHolder(mActivity, view);


    }

    @Override
    public void onBindViewHolder(MenuItemHolder menuItemHolder, int i) {
        DrawerMenuItem menuItem = mItems.get(i);
        menuItemHolder.mTitleTextView.setText(menuItem.getTitle());
        menuItemHolder.Intent = menuItem.getIntent();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
