package io.rendr.howyousay.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import io.rendr.howyousay.R;
import io.rendr.howyousay.utils.EnvironmentUtil;

/**
 * Created by ben on 1/13/16.
 * Copyright Rendr LLC All Rights Reserved
 */
public class DotIndicatorView extends View {

    private int mWidth;
    private int mHeight;
    private float mMiddleX;
    private float mMiddleY;
    private Paint mHighlightedPaint;
    private Paint mNormalPaint;

    int mCount;

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
        invalidate();
    }

    int mIndex;

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public DotIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaints();
    }



    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        mWidth = w;
        mHeight = h;
        mMiddleX = (float)mWidth/2f;
        mMiddleY = (float)mHeight/2f;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int dotRadius = (int)EnvironmentUtil.Screen.GetPxForDp(getResources(), 2);
        int space = (int)EnvironmentUtil.Screen.GetPxForDp(getResources(), 14);

        float curX = mMiddleX - (((float)getCount()/2) * space) + ((float)space/2f);

        for (int i = 0; i < getCount(); i++) {
            boolean isHighlighted = i == getIndex();
            canvas.drawCircle(curX, mMiddleY, isHighlighted?dotRadius * 2:dotRadius, isHighlighted ?mHighlightedPaint: mNormalPaint);
            curX += space;
        }

    }

    private void initPaints() {
        mHighlightedPaint = new Paint();
        mHighlightedPaint.setColor(getResources().getColor(R.color.white));
        mHighlightedPaint.setStrokeWidth(2);


        mNormalPaint = new Paint();
        mNormalPaint.setColor(getResources().getColor(R.color.rule_color));
        mNormalPaint.setStrokeWidth(2);


    }
}
