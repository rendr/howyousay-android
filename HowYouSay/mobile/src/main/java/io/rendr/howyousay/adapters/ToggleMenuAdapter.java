package io.rendr.howyousay.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import io.rendr.howyousay.R;
import io.rendr.howyousay.core.models.ToggleMenuItem;
import io.rendr.howyousay.viewholders.ToggleMenuItemHolder;

/**
 * Created by ben on 12/28/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class ToggleMenuAdapter extends RecyclerView.Adapter<ToggleMenuItemHolder> {

    private final ArrayList<ToggleMenuItem> mItems;
    private final Activity mActivity;


    public ToggleMenuAdapter(Activity activity, ArrayList<ToggleMenuItem> items) {
        mItems = items;
        mActivity = activity;

    }

    @Override
    public ToggleMenuItemHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mActivity);
        View view = layoutInflater.inflate(R.layout.settings_toggle_menu_item, viewGroup, false);
        return new ToggleMenuItemHolder(view);


    }

    @Override
    public void onBindViewHolder(ToggleMenuItemHolder menuItemHolder, int i) {
        ToggleMenuItem menuItem = mItems.get(i);
        menuItemHolder.mTitleTextView.setText(menuItem.getTitle());
        menuItemHolder.mCheckBox.setChecked(menuItem.getSelected());
        menuItemHolder.mMenuItem = menuItem;
    }

    public ArrayList<String> getSelectedTitles()
    {
        ArrayList<String> selectedTitles = new ArrayList<>();
        for (ToggleMenuItem item : mItems) {
            if(item.getSelected() == true)
                selectedTitles.add(item.getTitle());
        }
        return selectedTitles;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
