package io.rendr.howyousay.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.util.ArrayMap;
import android.view.View;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.UUID;

import io.rendr.howyousay.TranslationFragment;
import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 11/18/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class TranslationsViewPagerAdapter extends FragmentPagerAdapter {

    private long baseId = 0;

    private long numberOfTimesRefreshed = 0;

    private List<PhraseTranslation> mTranslations = new ArrayList<>();

    public void setTranslations(List<PhraseTranslation> translations){
        this.mTranslations = translations;
    }



    public TranslationsViewPagerAdapter(FragmentManager fm) {
        super(fm);


    }

    @Override
    public int getItemPosition(Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public Fragment getItem(int position) {

        return TranslationFragment.newInstance(mTranslations.get(position));
    }
    @Override
    public long getItemId(int position) {
        // give an ID different from position when position has been changed
        return baseId + position;
    }

    @Override
    public int getCount() {

        return mTranslations.size();
    }



    @Override
    public void notifyDataSetChanged() {
        numberOfTimesRefreshed++;
        baseId += getCount() + numberOfTimesRefreshed;
        super.notifyDataSetChanged();
    }
}
