package io.rendr.howyousay.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import io.rendr.howyousay.OnboardingAddPhrases;
import io.rendr.howyousay.OnboardingAddPhrases_;
import io.rendr.howyousay.OnboardingPageFragment;
import io.rendr.howyousay.OnboardingPageFragment_;


/**
 * Created by ben on 12/30/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class OnboardingViewPagerAdapter extends FragmentPagerAdapter {


    private final ArrayList<Fragment> mFrags;

    public OnboardingViewPagerAdapter(FragmentManager fm) {
        super(fm);
        mFrags = new ArrayList<>();
        mFrags.add(OnboardingAddPhrases_.newInstance());
        mFrags.add(OnboardingPageFragment_.newInstance());

    }

    @Override
    public Fragment getItem(int position) {

        return mFrags.get(position);
    }

    @Override
    public int getCount() {
        return mFrags.size();
    }
}
