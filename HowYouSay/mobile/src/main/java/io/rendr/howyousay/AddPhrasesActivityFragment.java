package io.rendr.howyousay;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.ArrayList;

import io.rendr.howyousay.adapters.ToggleMenuAdapter;
import io.rendr.howyousay.core.models.ToggleMenuItem;
import io.rendr.howyousay.core.services.AssetsService;

import io.rendr.howyousay.core.services.VocabSeedsAddPhrasesService;
import io.rendr.howyousay.core.services.VocabSeedsAvailableLanguagesService;
import io.rendr.howyousay.services.PermissionsService;

/**
 * A placeholder fragment containing a simple view.
 */
@EFragment(R.layout.fragment_add_phrases)
@OptionsMenu(R.menu.menu_add_phrases)
public class AddPhrasesActivityFragment extends android.support.v4.app.Fragment {


    @Bean
    AssetsService mAssetsService;

    @Bean
    VocabSeedsAvailableLanguagesService mVocabSeedsAvailableLanguagesService;

    @Bean
    VocabSeedsAddPhrasesService mVocabSeedsAddPhrasesService;

    @ViewById
    RecyclerView languagesRecyclerView;



    private ToggleMenuAdapter mLanguagesAdapter;

    public AddPhrasesActivityFragment() {
    }


    @AfterViews
    void initRecyclerView()
    {
        languagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLanguagesAdapter = new ToggleMenuAdapter(getActivity(), getToggleMenuItems());
        languagesRecyclerView.setAdapter(mLanguagesAdapter);
    }



    @OptionsItem(R.id.action_add_phrases)
    public void onAddPhrasesTouched()
    {
        if (!PermissionsService.hasWriteExternalStoragePermission(getActivity())) {
            PermissionsService.requestSeedPermissions(getActivity());
            return;
        }
        addPhrases();


    }



    protected void addPhrases() {
        ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "", getActivity().getString(R.string.adding_phrases));
        new AddPhrasesTask(progressDialog).execute();
    }


    @NonNull
    private ArrayList<ToggleMenuItem> getToggleMenuItems() {
        ArrayList<ToggleMenuItem> items = new ArrayList<>();
        ArrayList<String> languages = new ArrayList<>();
        try {
            languages = mVocabSeedsAvailableLanguagesService.get(mAssetsService, getActivity());
        }catch (IOException ex){
            Log.w("IOException Encountered", "Exception encountered trying to read languages");
        }
        for (String language : languages) {
            items.add(new ToggleMenuItem(language));
        }
        return items;
    }

    private class AddPhrasesTask extends AsyncTask {
        private ProgressDialog mProgressDialog;

        public AddPhrasesTask(ProgressDialog progressDialog) {

            mProgressDialog = progressDialog;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            getActivity().finish();
            Toast.makeText(getActivity(), R.string.phrases_added, Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            mVocabSeedsAddPhrasesService.add(mLanguagesAdapter.getSelectedTitles());
            return null;
        }
    }
}
