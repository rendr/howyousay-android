package io.rendr.howyousay.viewholders;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import io.rendr.howyousay.MainActivity;
import io.rendr.howyousay.PhraseActivity;
import io.rendr.howyousay.PhraseActivity_;
import io.rendr.howyousay.R;
import io.rendr.howyousay.core.models.Phrase;

/**
 * Created by ben on 11/17/15.
 */
public class PhraseItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public final Activity mActivity;
    public final TextView mTopTextView;
    public final TextView mBottomTextView;
    public final FrameLayout mSpacer;

    public Phrase phrase;

    public PhraseItemHolder(Activity activity, View itemView) {
        super(itemView);
        mActivity = activity;
        mTopTextView = (TextView)itemView.findViewById(R.id.topTextView);
        mBottomTextView = (TextView)itemView.findViewById(R.id.bottomTextView);
        mSpacer = (FrameLayout)itemView.findViewById(R.id.spacer);
        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(mActivity, PhraseActivity_.class);
        intent.putExtra(PhraseActivity.EXTRA_PHRASE_ID, phrase.getUUID().toString());
        mActivity.startActivityForResult(intent, MainActivity.RESULT_PHRASE_VIEW_COMPLETE);
    }

    public void flashHighlight(){
        itemView.setBackgroundColor(mActivity.getResources().getColor(R.color.highlight));
    }
}
