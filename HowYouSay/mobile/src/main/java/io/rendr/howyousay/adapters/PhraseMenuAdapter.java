package io.rendr.howyousay.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.UUID;

import io.rendr.howyousay.R;
import io.rendr.howyousay.core.models.Phrase;
import io.rendr.howyousay.viewholders.MenuItemHolder;
import io.rendr.howyousay.viewholders.PhraseItemHolder;

/**
 * Created by ben on 11/17/15.
 */
public class PhraseMenuAdapter extends RecyclerView.Adapter<PhraseItemHolder> {

    private final Activity mActivity;
    private final List<Phrase> mItems;

    public PhraseMenuAdapter(Activity activity, List<Phrase> items) {
        mActivity = activity;
        mItems = items;
    }

    @Override
    public PhraseItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mActivity);
        View view = layoutInflater.inflate(R.layout.phrase_list_item, parent, false);
        return new PhraseItemHolder(mActivity, view);
    }

    @Override
    public void onBindViewHolder(PhraseItemHolder holder, int position) {
        Phrase phrase = mItems.get(position);
        holder.mTopTextView.setText(phrase.getTitle());
        holder.mBottomTextView.setText(phrase.getTranslationTitle());
        holder.phrase = mItems.get(position);

        holder.mSpacer.setVisibility((position == mItems.size() - 1)?View.VISIBLE:View.GONE);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public UUID getItemUUID(int position)
    {
        return mItems.get(position).getUUID();
    }
}
