package io.rendr.howyousay;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.UUID;

import io.rendr.howyousay.adapters.TranslationsViewPagerAdapter;
import io.rendr.howyousay.core.util.StringUtil;
import io.rendr.howyousay.dialogs.DialogUtil;
import io.rendr.howyousay.core.models.Phrase;
import io.rendr.howyousay.core.models.PhraseTranslation;
import io.rendr.howyousay.core.services.PhraseService;
import io.rendr.howyousay.services.AnalyticsService;
import io.rendr.howyousay.services.TranslationAudioPlaybackService;
import io.rendr.howyousay.views.DotIndicatorView;

@EActivity(R.layout.activity_phrase)
public class PhraseActivity extends BasePhraseActivity implements ViewPager.PageTransformer {

    public static final String SCREEN_NAME = "Vocab Entry";
    @Bean
    protected PhraseService mPhraseService;

    @Bean
    protected AnalyticsService mAnalyticsService;

    public static final String EXTRA_PHRASE_ID = "id";
    public static final int RECORD_AUDIO_REQUEST = 1;

    private TranslationsViewPagerAdapter mPagerAdapter;

    private Phrase mPhrase;

    @ViewById(R.id.bookmarkButton)
    ImageButton bookmarkButton;

    @ViewById(R.id.recordPlaybackButton)
    ImageButton recordPlaybackButton;

    @ViewById(R.id.smallRecordButton)
    ImageButton smallRecordButton;

    @ViewById(R.id.pager)
    ViewPager mViewPager;

    @ViewById(R.id.phraseTitleText)
    EditText mPhraseTitleText;

    @ViewById(R.id.indicator)
    DotIndicatorView mDotIndicatorView;

    private TranslationAudioPlaybackService mTranslationAudioService;

//    boolean mChangesMade = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.getIntent().hasExtra(EXTRA_PHRASE_ID)) {
            UUID id = UUID.fromString(this.getIntent().getStringExtra(EXTRA_PHRASE_ID));
            mPhrase = mPhraseService.getPhrase(id);
        } else {
            mPhrase = new Phrase(UUID.randomUUID(), new ArrayList<PhraseTranslation>());
            mPhrase.getTranslations().add(new PhraseTranslation(UUID.randomUUID(), mPhrase.getUUID()));
        }
        initPlaybackService(0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_phrase, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        String phraseTitle = data.getStringExtra(TranslationRecordFragment.EXTRA_PHRASE_TITLE);

        mPhraseTitleText.setText(phraseTitle);
        getCurrentTranslationInViewPager().setTitle(data.getStringExtra(TranslationRecordFragment.EXTRA_TRANSLATION_TITLE));


    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mPhrase == null)
            return;


    }

    @Override
    protected void onStart() {
        super.onStart();
//        TranslationFragment.changesMade = false;
    }

    @Override
    public void finish() {

        if (mPhrase != null && !StringUtil.isNullOrEmpty(mPhrase.getTitle())) {

//            if(mChangesMade || TranslationFragment.changesMade) {
                mPhraseService.savePhrase(mPhrase);
                Toast.makeText(this, R.string.phrase_saved, Toast.LENGTH_SHORT).show();
//            }

            Intent returnIntent = new Intent();
            returnIntent.putExtra(EXTRA_PHRASE_ID, mPhrase.getUUID().toString());
            setResult(Activity.RESULT_OK, returnIntent);
        } else {
            setResult(Activity.RESULT_CANCELED, null);
        }
        super.finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        mPhraseTitleText.setText(mPhrase.getTitle());
        updateButtonStates();
        mAnalyticsService.trackScreen(SCREEN_NAME);
    }

    @Override
    public void transformPage(View page, float position) {
        int pageWidth = page.getWidth();

        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.


        } else if (position <= 1) { // [-1,1]

            float percentInPlace = 1 - Math.abs(position);
            page.setRotation(45 * position);
            page.setScaleX(percentInPlace);
            page.setScaleY(percentInPlace);
        } else { // (1,+Infinity]
            // This page is way off-screen to te right.


        }
    }

    @AfterViews
    protected void initViewPager() {

        mViewPager.setPageTransformer(true, this);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                initPlaybackService(position);
                mDotIndicatorView.setIndex(position);
                mDotIndicatorView.setCount(mViewPager.getAdapter().getCount());
                updateButtonStates();
            }

            @Override
            public void onPageSelected(int position) {

                initPlaybackService(position);
                mDotIndicatorView.setCount(mViewPager.getAdapter().getCount());
                mDotIndicatorView.setIndex(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateViewPager();
    }

    @AfterTextChange({R.id.phraseTitleText})
    public void onTitleChanged(TextView view) {
        String newText = mPhraseTitleText.getText().toString();
//        if (mPhrase != null && !StringUtil.isNullOrEmpty(mPhrase.getTitle()) && !mPhrase.getTitle().equals(newText)) {
            mPhrase.setTitle(newText);
//            mChangesMade = true;
//        }
    }

    @Click(R.id.bookmarkButton)
    public void toggleBookmark(View view) {
        mPhrase.setBookmarked(!mPhrase.isBookmarked());
        updateButtonStates();
    }

    @Click(R.id.recordPlaybackButton)
    public void toggleRecordPlayback(View view) {
        if (mTranslationAudioService.getRecordingAvailable()) {
            if (mTranslationAudioService.getIsPlaying()) {
                mTranslationAudioService.stopPlaying();
            } else {
                mTranslationAudioService.startPlaying();
            }
            updateButtonStates();
        } else {
            goPhraseAudio(view);
        }
    }

    @Click(R.id.addButton)
    public void addTranslation(View view) {
        mPhrase.getTranslations().add(mViewPager.getCurrentItem() + 1, new PhraseTranslation(UUID.randomUUID(), mPhrase.getUUID()));
        updateViewPager();
    }

    @Click(R.id.smallRecordButton)
    public void goPhraseAudio(View view) {
        Intent intent = new Intent(this, PhraseAudioActivity_.class);


        intent.putExtra(PhraseAudioActivity.EXTRA_PHRASE, mPhrase);
        intent.putExtra(BaseTranslationFragment.EXTRA_TRANSLATION, getCurrentTranslationInViewPager());
        startActivityForResult(intent, RECORD_AUDIO_REQUEST);
    }

    @Click(R.id.deleteButton)
    public void delete(View view) {
        displayDeleteDialog();
    }

    @Click(R.id.backArrowButton)
    public void goBack(View view) {
        finish();
    }


    protected PhraseTranslation getCurrentTranslationInViewPager() {
        return mPhrase.getTranslations().get(mViewPager.getCurrentItem());
    }


    private void displayDeleteDialog() {
        if (mPhrase.getTranslations().size() > 1) {
            DialogUtil.ShowPhraseTranslationDeleteMenu(this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case -1:
                            deletePhrase();
                            break;
                        case -2:
                            deleteTranslation();
                            break;
                    }
                }
            });
        } else {
            DialogUtil.ShowPhraseDeleteMenu(this, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case -1:
                            deletePhrase();
                            break;
                    }
                }
            });
        }
    }

    private void deleteTranslation() {
        int translationIndex = mViewPager.getCurrentItem();
        PhraseTranslation translation = mPhrase.getTranslations().get(translationIndex);
        mPhraseService.deleteTranslation(translation.getUUID());
        mPhrase.getTranslations().remove(translationIndex);
        updateViewPager();
    }

    private void deletePhrase() {
        mPhraseService.deletePhrase(mPhrase.getUUID());
        mPhrase = null;
        finish();
    }

    protected TranslationFragment getCurrentTransFragment() {
        return (TranslationFragment) ((FragmentPagerAdapter) mViewPager.getAdapter()).getItem(mViewPager.getCurrentItem());
    }

    public void updateButtonStates() {
        if (mPhrase == null)
            return;
        boolean recordingAvailable = mTranslationAudioService.getRecordingAvailable();
        smallRecordButton.setVisibility(recordingAvailable ? View.VISIBLE : View.INVISIBLE);
        if (recordingAvailable) {
            recordPlaybackButton.setImageResource(mTranslationAudioService.getIsPlaying() ? R.drawable.button_solid_stop : R.drawable.button_solid_play);
        } else {
            recordPlaybackButton.setImageResource(R.drawable.button_record);
        }
        bookmarkButton.setImageResource(mPhrase.isBookmarked() ? R.drawable.button_bookmark_active : R.drawable.button_bookmark);
    }


    protected void updateViewPager() {
        if (mPagerAdapter == null) {
            mPagerAdapter = new TranslationsViewPagerAdapter(getSupportFragmentManager());
            mViewPager.setAdapter(mPagerAdapter);
        }
        mPagerAdapter.setTranslations(mPhrase.getTranslations());
        mPagerAdapter.notifyDataSetChanged();

        int nextIndex = Math.min(mViewPager.getCurrentItem() + 1, mPhrase.getTranslations().size() - 1);

        mViewPager.setCurrentItem(nextIndex, true);

    }


    protected void initPlaybackService(int position) {
        mTranslationAudioService = new TranslationAudioPlaybackService(mPhrase.getTranslations().get(position).getUUID(), new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                updateButtonStates();
            }
        }, this);
    }

}
