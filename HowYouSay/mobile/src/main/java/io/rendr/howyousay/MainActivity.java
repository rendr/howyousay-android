package io.rendr.howyousay;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.UUID;

public class MainActivity extends FragmentActivity {

    public static int RESULT_PHRASE_VIEW_COMPLETE = 1;

    private Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getSupportFragmentManager();
        mFragment = fm.findFragmentById(R.id.fragment_container);

        if(mFragment == null) {
            mFragment = new MainActivityFragment_();
            fm.beginTransaction().add(R.id.fragment_container, mFragment).commit();

        }


    }



    public void goAddPhrase(View view){
        startActivityForResult(new Intent(this, PhraseActivity_.class), RESULT_PHRASE_VIEW_COMPLETE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

//        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && data!=null && data.hasExtra(PhraseActivity.EXTRA_PHRASE_ID)){
            ((MainActivityFragment)mFragment).scrollToPhrase(UUID.fromString(data.getStringExtra(PhraseActivity.EXTRA_PHRASE_ID)));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_phrases) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
