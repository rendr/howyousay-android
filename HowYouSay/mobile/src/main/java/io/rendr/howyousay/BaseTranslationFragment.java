package io.rendr.howyousay;

import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 12/2/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public abstract class BaseTranslationFragment extends Fragment {

    public static final String EXTRA_TRANSLATION = "translation";




    protected PhraseTranslation getTranslation()
    {

        return (PhraseTranslation)getArguments().getSerializable(BaseTranslationFragment.EXTRA_TRANSLATION);
    }





    protected abstract void updateButtonStates();


}
