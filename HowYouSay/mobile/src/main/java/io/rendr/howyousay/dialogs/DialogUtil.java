package io.rendr.howyousay.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import io.rendr.howyousay.R;

/**
 * Created by ben on 12/4/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class DialogUtil {

    public static void ShowPhraseTranslationDeleteMenu(Context context,  Dialog.OnClickListener clickListener){
        new AlertDialog.Builder(context)
                .setTitle(R.string.what_to_delete)
                .setPositiveButton(R.string.whole_phrase, clickListener)
                .setNeutralButton(R.string.cancel, clickListener)
                .setNegativeButton(R.string.the_translation, clickListener)
                .create()
                .show();
    }

    public static void ShowPhraseDeleteMenu(Context context,  Dialog.OnClickListener clickListener){
        new AlertDialog.Builder(context)
                .setTitle(R.string.are_you_sure)
                .setMessage(R.string.deleting_cannot_be_undone)
                .setPositiveButton(R.string.yes_continue, clickListener)
                .setNeutralButton(R.string.cancel, clickListener)
                .create()
                .show();
    }



}
