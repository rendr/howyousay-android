package io.rendr.howyousay;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;

import io.rendr.howyousay.services.AnalyticsService;

@EActivity(R.layout.activity_languages)
public class LanguagesActivity extends AppCompatActivity {

    public static final String SCREEN_NAME = "Languages View";

    @Bean
    protected AnalyticsService mAnalyticsService;

    @ViewById(R.id.toolbar)
    protected Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @AfterViews
    public void initToolbar()
    {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAnalyticsService.trackScreen(SCREEN_NAME);
    }

    @OptionsItem(android.R.id.home)
    public void goHome()
    {
        this.finish();
    }

}
