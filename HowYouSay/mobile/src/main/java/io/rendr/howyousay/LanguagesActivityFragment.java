package io.rendr.howyousay;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.ArrayList;

import io.rendr.howyousay.adapters.ToggleMenuAdapter;
import io.rendr.howyousay.core.models.ToggleMenuItem;
import io.rendr.howyousay.core.services.LanguagePreferencesService;
import io.rendr.howyousay.core.services.PhraseService;

/**
 * A placeholder fragment containing a simple view.
 */
@EFragment(R.layout.fragment_languages)
public class LanguagesActivityFragment extends android.support.v4.app.Fragment {

    @Bean
    PhraseService mPhraseService;

    @Bean
    LanguagePreferencesService mLanguagePreferencesService;

    @ViewById(R.id.languagesRecyclerView)
    RecyclerView mRecyclerView;

    private ToggleMenuAdapter mLanguagesAdapter;
    private ArrayList<ToggleMenuItem> mItems;

    public LanguagesActivityFragment() {
    }


    @AfterViews
    protected void initRecyclerView()
    {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLanguagesAdapter = new ToggleMenuAdapter(getActivity(), getToggleMenuItems());
        mRecyclerView.setAdapter(mLanguagesAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        for (ToggleMenuItem item : mItems) {
            mLanguagePreferencesService.setLanguageDisabled(item.getTitle(), !item.getSelected());
        }
        Toast.makeText(getActivity(), R.string.enabled_languages_updated, Toast.LENGTH_SHORT).show();

    }

    @NonNull
    private ArrayList<ToggleMenuItem> getToggleMenuItems() {
        mItems = new ArrayList<>();
        ArrayList<String> languages = mPhraseService.getLanguages();

        for (String language : languages) {
            mItems.add(new ToggleMenuItem(language, !mLanguagePreferencesService.isLanguageDisabled(language)));
        }
        return mItems;
    }
}
