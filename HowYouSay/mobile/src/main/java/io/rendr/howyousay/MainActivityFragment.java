package io.rendr.howyousay;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;

import java.io.Console;
import java.util.UUID;

import io.rendr.howyousay.adapters.MainViewPagerAdapter;
import io.rendr.howyousay.services.AnalyticsService;
import io.rendr.howyousay.utils.EnvironmentUtil;

/**
 * A placeholder fragment containing a simple view.
 */
@EFragment
public class MainActivityFragment extends android.support.v4.app.Fragment implements SearchView.OnQueryTextListener {

    public static final String SCREEN_NAME = "Vocabulary List";

    @Bean
    AnalyticsService mAnalyticsService;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ViewPager mPager;
    private SearchView mSearchView;
    private MainViewPagerAdapter mPagerAdapter;
    private MainMenuFragment_ mMenuFragment;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setMenuButtonListener();
        setBackButtonListener();
        styleSearchView();
        initViewPager();
        initViewPager();
        initTabs();

    }

    private void initTabs() {

        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) getView().findViewById(R.id.tabStrip);
        tabStrip.setShouldExpand(true);
        tabStrip.setViewPager(mPager);
        tabStrip.setTextAppearance(R.style.TabText);
        tabStrip.setIndicatorColorResource(R.color.highlight);
        tabStrip.setIndicatorHeight((int) EnvironmentUtil.Screen.GetPxForDp(getResources(), 3));


    }

    private void initViewPager() {
        mPager = (ViewPager) getView().findViewById(R.id.pager);
        mPagerAdapter = new MainViewPagerAdapter(this.getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }

    private void setBackButtonListener() {
        ImageButton.OnClickListener listener = new ImageButton.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mDrawerLayout = (DrawerLayout) getView().findViewById(R.id.drawer_layout);
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
        };
        getView().findViewById(R.id.backArrowButton).setOnClickListener(listener);
    }

    private void setMenuButtonListener() {


        ImageButton.OnClickListener listener = new ImageButton.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                mDrawerLayout = (DrawerLayout) getView().findViewById(R.id.drawer_layout);
                mDrawerLayout.openDrawer(Gravity.LEFT);
                mAnalyticsService.trackScreen("Settings View");
            }
        };
        getView().findViewById(R.id.menuButton).setOnClickListener(listener);

    }

    @Override
    public void onStart() {
        super.onStart();
        mMenuFragment = new MainMenuFragment_();
        this.getChildFragmentManager().beginTransaction().add(R.id.menu_drawer, mMenuFragment).commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAnalyticsService.trackScreen(SCREEN_NAME);
    }

    protected void styleSearchView() {

        mSearchView = (SearchView) this.getView().findViewById(R.id.searchView);
        int searchTextViewId = mSearchView.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView searchTextView = (TextView) mSearchView.findViewById(searchTextViewId);

        searchTextView.setTextColor(getResources().getColor(R.color.header_foreground));
        int closeButtonId = mSearchView.getContext().getResources().getIdentifier("android:id/search_close_btn", null, null);
        ImageView searchClose = (ImageView) mSearchView.findViewById(closeButtonId);
        searchClose.setImageResource(R.drawable.icon_close);

        mSearchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mPagerAdapter.setSearchCriteria(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mPagerAdapter.setSearchCriteria(newText);
        return false;
    }

    public void scrollToPhrase(UUID uuid) {
        ((PhraseListFragment)mPagerAdapter.getItem(0)).scrollToPhrase(uuid);
    }
}
