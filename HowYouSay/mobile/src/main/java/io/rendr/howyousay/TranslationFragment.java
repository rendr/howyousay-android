package io.rendr.howyousay;


import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

import io.rendr.howyousay.core.models.PhraseTranslation;


/**
 * A simple {@link Fragment} subclass.
 */
@EFragment
public class TranslationFragment extends BaseTranslationFragment {


//    public static boolean changesMade = false;


    public static TranslationFragment newInstance(PhraseTranslation translation) {
        TranslationFragment frag = new TranslationFragment_();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BaseTranslationFragment.EXTRA_TRANSLATION, translation);
        frag.setArguments(bundle);
        return frag;
    }


    public TranslationFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_translation, container, false);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onResume() {
        super.onResume();
        updateEditTextFields();

    }

    @ViewById(R.id.titleTextView)
    EditText titleTextView;

    @ViewById(R.id.phoneticTextView)
    EditText phoneticTextView;

    @ViewById(R.id.languageTextView)
    EditText languageTextView;

    @ViewById(R.id.tagsTextView)
    EditText tagsTextView;

    @ViewById(R.id.notesTextView)
    EditText notesTextView;


    @AfterTextChange({R.id.titleTextView})
    void updateTitle(TextView textView) {
        String newText = titleTextView.getText().toString();
//        if (!newText.equals(getTranslation().getTitle())) {
            getTranslation().setTitle(newText);
//            changesMade = true;
//        }
    }

    @AfterTextChange({R.id.phoneticTextView})
    void updatePhonetic(TextView textView) {
        String newText = phoneticTextView.getText().toString();
//        if (!newText.equals(getTranslation().getPhonetic())) {
            getTranslation().setPhonetic(newText);
//            changesMade = true;
//        }
    }

    @AfterTextChange({R.id.languageTextView})
    void updateLanguage(TextView textView) {

        String newText = languageTextView.getText().toString();
//        if(!newText.equals(getTranslation().getLanguageTitle())) {
            getTranslation().setLanguageTitle(newText);
//            changesMade = true;
//        }
    }

    @AfterTextChange({R.id.tagsTextView})
    void updateTags(TextView textView) {

        String newText = tagsTextView.getText().toString();
//        if(!newText.equals(getTranslation().getTags())) {
            getTranslation().setTags(newText);
//            changesMade = true;
//        }
    }

    @AfterTextChange({R.id.notesTextView})
    void updateNotes(TextView textView) {
        String newText = notesTextView.getText().toString();
//        if(!newText.equals(getTranslation().getNotes())) {
            getTranslation().setNotes(newText);
//            changesMade = true;
//        }
    }

    @AfterViews
    protected void updateEditTextFields() {
        PhraseTranslation translation = getTranslation();
        titleTextView.setText(translation.getTitle());
        phoneticTextView.setText(translation.getPhonetic());
        languageTextView.setText(translation.getLanguageTitle());
        tagsTextView.setText(translation.getTags());
        notesTextView.setText(translation.getNotes());

    }


    @Override
    protected void updateButtonStates() {
        ((PhraseActivity) getActivity()).updateButtonStates();
    }
}
