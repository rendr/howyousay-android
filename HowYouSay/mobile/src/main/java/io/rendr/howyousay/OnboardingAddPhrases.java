package io.rendr.howyousay;


import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.ArrayList;

import io.rendr.howyousay.adapters.ToggleMenuAdapter;
import io.rendr.howyousay.core.models.ToggleMenuItem;
import io.rendr.howyousay.core.services.AssetsService;
import io.rendr.howyousay.core.services.VocabSeedsAddPhrasesService;
import io.rendr.howyousay.core.services.VocabSeedsAvailableLanguagesService;
import io.rendr.howyousay.services.AnalyticsService;
import io.rendr.howyousay.services.PermissionsService;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OnboardingAddPhrases#newInstance} factory method to
 * create an instance of this fragment.
 */
@EFragment(R.layout.fragment_onboarding_add_phrases)
public class OnboardingAddPhrases extends android.support.v4.app.Fragment {

    @ViewById
    VideoView videoView;

    @Bean
    AssetsService mAssetsService;

    @Bean
    AnalyticsService mAnalyticsService;

    @Bean
    VocabSeedsAvailableLanguagesService mVocabSeedsAvailableLanguagesService;

    @Bean
    VocabSeedsAddPhrasesService mVocabSeedsAddPhrasesService;

    @ViewById
    RecyclerView languagesRecyclerView;

    private ToggleMenuAdapter mLanguagesAdapter;

    public OnboardingAddPhrases() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment OnboardingAddPhrases.
     */

    public static OnboardingAddPhrases newInstance() {
        OnboardingAddPhrases fragment = new OnboardingAddPhrases_();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @AfterViews
    void initRecyclerView()
    {
        languagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mLanguagesAdapter = new ToggleMenuAdapter(getActivity(), getToggleMenuItems());
        languagesRecyclerView.setAdapter(mLanguagesAdapter);
    }

    @AfterViews
    void initVideo() {
        String path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.video_2;
        videoView.setVideoURI(Uri.parse(path));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView.start();
    }

    @Click(R.id.addButton)
    public void onAddPhrasesTouched()
    {
        if (!PermissionsService.hasWriteExternalStoragePermission(getActivity())) {
            PermissionsService.requestSeedPermissions(getActivity());
            return;
        }
        addPhrases();


    }

    @Override
    public void onResume() {
        super.onResume();
        mAnalyticsService.trackScreen("Seed Vocabulary View");
    }

    protected void addPhrases() {
        ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "", getActivity().getString(R.string.adding_phrases));
        new AddPhrasesTask(progressDialog).execute();
    }

    @NonNull
    private ArrayList<ToggleMenuItem> getToggleMenuItems() {
        ArrayList<ToggleMenuItem> items = new ArrayList<>();
        ArrayList<String> languages = new ArrayList<>();
        try {
            languages = mVocabSeedsAvailableLanguagesService.get(mAssetsService, getActivity());
        }catch (IOException ex){
            Log.w("IOException Encountered", "Exception encountered trying to read languages");
        }
        for (String language : languages) {
            items.add(new ToggleMenuItem(language));
        }
        return items;
    }

    private class AddPhrasesTask extends AsyncTask {
        private ProgressDialog mProgressDialog;

        public AddPhrasesTask(ProgressDialog progressDialog) {

            mProgressDialog = progressDialog;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            ((SplashActivity)getActivity()).next();
            Toast.makeText(getActivity(), R.string.phrases_added, Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            mVocabSeedsAddPhrasesService.add(mLanguagesAdapter.getSelectedTitles());
            return null;
        }
    }
}
