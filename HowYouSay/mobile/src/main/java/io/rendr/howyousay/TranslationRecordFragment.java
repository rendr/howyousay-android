package io.rendr.howyousay;

import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.autofit.et.lib.AutoFitEditText;

import java.util.UUID;

import io.rendr.howyousay.core.models.PhraseTranslation;
import io.rendr.howyousay.services.PermissionsService;
import io.rendr.howyousay.services.TranslationAudioPlaybackService;
import io.rendr.howyousay.services.TranslationAudioRecordingService;
import io.rendr.howyousay.views.VisualizerView;

/**
 * Created by ben on 12/2/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class TranslationRecordFragment extends Fragment {

    public static final String EXTRA_PHRASE_TITLE = "extraPhraseTitle";
    public static final String EXTRA_TRANSLATION_TITLE = "extraTranslationTitle";
    public static final String EXTRA_TRANSLATION_UUID = "extraTranslationUUID";
    public static final int REPEAT_INTERVAL = 40;

    //controls
    protected ImageButton mRecordButton;
    protected ImageButton mPlayStopButton;
    protected AutoFitEditText mPhraseTitleTextView;
    protected AutoFitEditText mTranslationTitleTextView;
    protected ImageButton mDeleteButton;
    private VisualizerView mVisualizerView;
    private Handler mHandler;
    private Visualizer mVisualizer;


    private String getPhraseTitle() {
        return getArguments().getString(EXTRA_PHRASE_TITLE);
    }

    private String getTranslationTitle() {
        return getArguments().getString(EXTRA_TRANSLATION_TITLE);
    }

    private UUID getTranslationUUID() {
        return UUID.fromString(getArguments().getString(EXTRA_TRANSLATION_UUID));
    }

    public static TranslationRecordFragment newInstance(String phraseTitle, String translationTitle, UUID translationId) {
        TranslationRecordFragment frag = new TranslationRecordFragment();
        Bundle bundle = new Bundle();

        bundle.putString(EXTRA_PHRASE_TITLE, phraseTitle);
        bundle.putString(EXTRA_TRANSLATION_TITLE, translationTitle);
        bundle.putString(EXTRA_TRANSLATION_UUID, translationId.toString());
        frag.setArguments(bundle);
        return frag;
    }

    private TranslationAudioPlaybackService mTranslationService;

    public TranslationAudioPlaybackService getTranslationAudioService() {
        if (mTranslationService == null)
            mTranslationService = new TranslationAudioPlaybackService(getTranslationUUID(), new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    updateButtonStates();
                }
            }, getActivity());
        return mTranslationService;
    }

    private TranslationAudioRecordingService mRecordingService;

    public TranslationAudioRecordingService getRecordingService() {
        if (mRecordingService == null)
            mRecordingService = new TranslationAudioRecordingService(getTranslationUUID());
        return mRecordingService;
    }

    public TranslationRecordFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_translation_record, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mVisualizerView = (VisualizerView) getView().findViewById(R.id.visualizer);
        mRecordButton = (ImageButton) getView().findViewById(R.id.recordButton);
        mPlayStopButton = (ImageButton) getView().findViewById(R.id.playStopButton);
        mDeleteButton = (ImageButton) getView().findViewById(R.id.deleteButton);
        mPhraseTitleTextView = (AutoFitEditText) getView().findViewById(R.id.phraseTitleText);
        mTranslationTitleTextView = (AutoFitEditText) getView().findViewById(R.id.translationTitleText);

        mHandler = new Handler();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateButtonStates();
        mTranslationTitleTextView.setText(getTranslationTitle());
        mPhraseTitleTextView.setText(getPhraseTitle());

    }


    public void toggleRecord(View view) {


        if (!PermissionsService.hasMicPermission(getActivity()) || !PermissionsService.hasReadExternalStoragePermission(getActivity()) || !PermissionsService.hasWriteExternalStoragePermission(getActivity())) {
            PermissionsService.requestRequiredRecordingPermissions(getActivity());
            return;
        }

        if (!getRecordingService().isRecording()) {
            getRecordingService().startRecording();
            mHandler.post(updateVisualizer);
            mVisualizerView.clear();
        } else {
            getRecordingService().stopRecording();
            mHandler.removeCallbacks(updateVisualizer);
            mVisualizerView.clear();
        }

        updateButtonStates();
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.recordButton:
                toggleRecord(view);
                break;
            case R.id.playStopButton:
                togglePlay(view);
                break;
            case R.id.deleteButton:
                delete(view);
                break;
        }
    }


    protected void delete(View view) {
        getRecordingService().delete();
        updateButtonStates();
    }

    protected void togglePlay(View view) {
        if (getTranslationAudioService().getIsPlaying()) {
            getTranslationAudioService().stopPlaying();
            stopPlayBackVisualizer();
        } else {
            getTranslationAudioService().startPlaying();
            startPlayBackVisualizer();

        }
        updateButtonStates();
    }

    private void startPlayBackVisualizer() {
        mVisualizerView.clear();
        mVisualizer = new Visualizer(getTranslationAudioService().getPlayer().getAudioSessionId());
        mVisualizer.setEnabled(true);
        mVisualizer.setScalingMode(Visualizer.SCALING_MODE_NORMALIZED);
        mHandler.post(updateVisualizer);
    }

    private void stopPlayBackVisualizer() {
        if(mVisualizer == null)
            return;
        mVisualizer.setEnabled(false);
        mHandler.removeCallbacks(updateVisualizer);
        mVisualizerView.clear();
    }


    protected boolean isPlaybackDeleteEnabled() {
        return getTranslationAudioService().getRecordingAvailable() && !getRecordingService().isRecording();
    }


    protected void updateButtonStates() {
        mRecordButton.setImageResource(getRecordingService().isRecording() ? R.drawable.button_record_active : R.drawable.button_record_big);
        mPlayStopButton.setImageResource(getTranslationAudioService().getIsPlaying() ? R.drawable.button_stop : R.drawable.button_play);
        mPlayStopButton.setEnabled(isPlaybackDeleteEnabled());
        mDeleteButton.setEnabled(isPlaybackDeleteEnabled());
        mPlayStopButton.setAlpha(isPlaybackDeleteEnabled() ? 1 : .25f);
        mDeleteButton.setAlpha(isPlaybackDeleteEnabled() ? 1 : .25f);
        if(!getTranslationAudioService().getIsPlaying() && !getRecordingService().isRecording()){
            stopPlayBackVisualizer();
        }

    }


    // updates the visualizer every 50 milliseconds
    Runnable updateVisualizer = new Runnable() {
        @Override
        public void run() {
            if (getRecordingService().isRecording()) // if we are already recording
            {
                // get the current amplitude
                int x = getRecordingService().getRecorder().getMaxAmplitude();
                mVisualizerView.addAmplitude(x); // update the VisualizeView
                mVisualizerView.invalidate(); // refresh the VisualizerView

                // update in 40 milliseconds
                mHandler.postDelayed(this, REPEAT_INTERVAL);


            }

            if (getTranslationAudioService().getIsPlaying()) {

                byte[] waveData = new byte[mVisualizer.getCaptureSize()];
                mVisualizer.getWaveForm(waveData);
                int minValue = 128;
                int maxValue = 0;
                for (byte d : waveData) {
                    int reading = Math.abs((int) d);
                    minValue = Math.min(minValue, reading);
                    maxValue = Math.max(maxValue, reading);
                }
                int delta = maxValue - minValue;
                mVisualizerView.addIntensity((delta / 120f));
                mVisualizerView.invalidate();


                mHandler.postDelayed(this, REPEAT_INTERVAL);
            }


        }
    };


}
