package io.rendr.howyousay.services;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import io.rendr.howyousay.R;
import io.rendr.howyousay.core.services.BaseTranslationAudioService;

/**
 * Created by ben on 12/4/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class TranslationAudioPlaybackService extends BaseTranslationAudioService {


    protected static final String LOG_TAG_RECORDING_AVAILABLE = "getRecoringAvailable";
    protected static final String LOG_TAG_AUDIO_PLAYBACK = "AudioRecordTest";
    private final MediaPlayer.OnCompletionListener mCompletionListener;
    private final Context mContext;
    private final AudioManager mAudioManager;
    protected MediaPlayer mPlayer;
    private Handler mHandler;

    public MediaPlayer getPlayer()
    {
        return mPlayer;
    }


    public TranslationAudioPlaybackService(UUID translationUUID, MediaPlayer.OnCompletionListener mediaPlayerCompletionListener, Context context) {
        super(translationUUID);
        mPlayer = new MediaPlayer();
        mContext = context;
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mCompletionListener = mediaPlayerCompletionListener;
        mHandler = new Handler();
    }

    public boolean getRecordingAvailable(){
        String audioFilePath = getAudioFilePath();

        return new File(audioFilePath).exists();

    }


    public boolean getIsPlaying()
    {
        if(mPlayer == null){
            return false;
        }
        return mPlayer.isPlaying();
    }

    public void startPlaying() {
        mPlayer = new MediaPlayer();
        mPlayer.setVolume(getStreamVolume(), getStreamVolume());

        try {
            mPlayer.setDataSource(getAudioFilePath());
            mPlayer.prepare();
            mPlayer.setOnCompletionListener(mCompletionListener);
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mPlayer.start();

            mHandler.post(updateVolume);

            if(getStreamVolume() == 0)
                Toast.makeText(mContext, R.string.phone_muted, Toast.LENGTH_SHORT).show();


        } catch (IOException e) {
            Log.e(LOG_TAG_AUDIO_PLAYBACK, "prepare() failed");
        }
    }

    protected float getStreamVolume() {
        return (float)mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC)/15f;
    }

    public void stopPlaying() {
        if(mPlayer != null) {
            mPlayer.release();
            mPlayer = null;

        }
        mHandler.removeCallbacks(updateVolume);

    }

    Runnable updateVolume = new Runnable() {
        @Override
        public void run() {
            Log.w("Volume:", Float.toString(getStreamVolume()));
           if(mPlayer != null)
               mPlayer.setVolume(getStreamVolume(),getStreamVolume());

            mHandler.postDelayed(this, 50);

            if(!getIsPlaying())
                mHandler.removeCallbacks(updateVolume);

        }
    };


}
