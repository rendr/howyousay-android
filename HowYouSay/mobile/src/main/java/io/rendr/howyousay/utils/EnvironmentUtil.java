package io.rendr.howyousay.utils;

import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by ben on 11/17/15.
 */
public class EnvironmentUtil {
    public static class Screen{
        public static float GetPxForDp(Resources r, int dp)
        {

            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        }
    }
}
