package io.rendr.howyousay.services;

import android.media.MediaRecorder;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import io.rendr.howyousay.core.services.BaseTranslationAudioService;

/**
 * Created by ben on 12/5/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class TranslationAudioRecordingService extends BaseTranslationAudioService {





    protected MediaRecorder mRecorder;

    public MediaRecorder getRecorder()
    {
        return mRecorder;
    }

    private static final String RECORDING_LOG_TAG = "recordingTranslation";

    private boolean mRecording = false;



    public TranslationAudioRecordingService(UUID translationUUID) {
        super(translationUUID);


    }

    public boolean isRecording(){
        return mRecording;
    }

    public void startRecording() {
        mRecorder = new MediaRecorder();


        try {
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mRecorder.setOutputFile(getAudioFilePath());
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            try {
                mRecorder.prepare();
            } catch (IOException e) {
                Log.e(RECORDING_LOG_TAG, "prepare() failed");
            }

            mRecorder.start();
            mRecording = true;


        } catch (Exception ex) {
            stopRecording();

        }

    }

    public void stopRecording() {
        mRecording = false;
        mRecorder.stop();
        mRecorder.release();
        mRecorder = null;


    }

    public void delete() {
        new File(getAudioFilePath()).delete();
    }
}
