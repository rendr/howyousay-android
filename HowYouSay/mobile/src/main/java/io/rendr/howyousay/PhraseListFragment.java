package io.rendr.howyousay;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;

import java.util.UUID;

import io.rendr.howyousay.adapters.PhraseMenuAdapter;
import io.rendr.howyousay.core.services.PhraseService;
import io.rendr.howyousay.viewholders.PhraseItemHolder;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment
public class PhraseListFragment extends Fragment {

    @Bean
    protected PhraseService phraseService;

    public static final String EXTRA_SHOW_BOOKMARKED = "showBookmarked";
    private boolean mShowBookmarked;
    private android.support.v7.widget.RecyclerView mRecyclerView;
    private String mSearchCriteria = "";
    private UUID mPhraseIdToScrollTo;

    public PhraseListFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phrase_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.phraseRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mShowBookmarked = this.getArguments().getBoolean(EXTRA_SHOW_BOOKMARKED);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateRecyclerAdapter();
        if(mPhraseIdToScrollTo != null){
            for (int i = 0; i < mRecyclerView.getAdapter().getItemCount(); i++) {
                UUID itemUUID = ((PhraseMenuAdapter) mRecyclerView.getAdapter()).getItemUUID(i);
                if(itemUUID.equals(mPhraseIdToScrollTo)){


                    PhraseItemHolder holder = (PhraseItemHolder)mRecyclerView.findViewHolderForAdapterPosition(i);
                    mRecyclerView.scrollToPosition(i);

                    break;
                }
            }
        }


    }

    public void scrollToPhrase(UUID phraseID)
    {
        mPhraseIdToScrollTo = phraseID;
    }

    public static PhraseListFragment newInstance(boolean showBookmarked) {
        PhraseListFragment frag = new PhraseListFragment_();
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_SHOW_BOOKMARKED, showBookmarked);
        frag.setArguments(args);
        return frag;
    }



    public void setSearchCriteria(String criteria)
    {
        mSearchCriteria = criteria;
        updateRecyclerAdapter();
    }

    protected void updateRecyclerAdapter() {
        if (!mShowBookmarked) {
            mRecyclerView.setAdapter(new PhraseMenuAdapter(getActivity(), phraseService.getPhrases(mSearchCriteria)));
        } else {
            mRecyclerView.setAdapter(new PhraseMenuAdapter(getActivity(), phraseService.getBookmarkedPhrases(mSearchCriteria)));
        }
    }
}
