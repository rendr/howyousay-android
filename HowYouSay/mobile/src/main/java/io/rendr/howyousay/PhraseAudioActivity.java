package io.rendr.howyousay;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

import com.autofit.et.lib.AutoFitEditText;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

import java.util.UUID;

import io.rendr.howyousay.core.models.Phrase;
import io.rendr.howyousay.core.models.PhraseTranslation;
import io.rendr.howyousay.core.services.PhraseService;
import io.rendr.howyousay.services.AnalyticsService;
import io.rendr.howyousay.services.PermissionsService;

@EActivity
public class PhraseAudioActivity extends BasePhraseActivity {

    public static final String SCREEN_NAME = "Audio View";

    @Bean
    protected AnalyticsService mAnalyticsService;


    //constants
    public static final String EXTRA_PHRASE = "extraPhrase";
    private TranslationRecordFragment mTranslationRecordFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phrase_audio);



        mPhrase = (Phrase)getIntent().getSerializableExtra(EXTRA_PHRASE);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


        PhraseTranslation translation = (PhraseTranslation) this.getIntent().getSerializableExtra(BaseTranslationFragment.EXTRA_TRANSLATION);
        mTranslationRecordFragment = TranslationRecordFragment.newInstance(mPhrase.getTitle(), translation.getTitle(), translation.getUUID());
        fragmentTransaction.add(R.id.fragment_container, mTranslationRecordFragment);
        fragmentTransaction.commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
        mAnalyticsService.trackScreen(SCREEN_NAME);
    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    public void finish() {
        Intent returnIntent = new Intent();

        String phraseTitle = ((AutoFitEditText) findViewById(R.id.phraseTitleText)).getText().toString();
        returnIntent.putExtra(TranslationRecordFragment.EXTRA_PHRASE_TITLE, phraseTitle);
        returnIntent.putExtra(TranslationRecordFragment.EXTRA_TRANSLATION_TITLE, ((AutoFitEditText) findViewById(R.id.translationTitleText)).getText().toString());

        setResult(Activity.RESULT_OK, returnIntent);
        super.finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mTranslationRecordFragment.toggleRecord(null);
        } else {
            PermissionsService.displayRecordPermissionsNeededDialog(this);
        }
    }

    public void onClick(View view)
    {
        mTranslationRecordFragment.onClick(view);
    }


    public void goBack(View view) {
        finish();
    }



}
