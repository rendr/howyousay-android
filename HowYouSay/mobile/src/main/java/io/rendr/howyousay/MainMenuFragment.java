package io.rendr.howyousay;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;

import java.util.ArrayList;

import io.rendr.howyousay.adapters.MainMenuAdapter;
import io.rendr.howyousay.core.models.DrawerMenuItem;
import io.rendr.howyousay.core.services.PhraseService;

/**
 * A placeholder fragment containing a simple view.
 */
@EFragment
public class MainMenuFragment extends Fragment {

    @Bean
    PhraseService mPhraseService;

    private RecyclerView mSettingsRecyclerView;
    private RecyclerView mAboutRecyclerView;

    public MainMenuFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        mSettingsRecyclerView = (RecyclerView)view.findViewById(R.id.settingsRecyclerView);
        mSettingsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));



        mAboutRecyclerView =(RecyclerView)view.findViewById(R.id.aboutRecyclerView);
        mAboutRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mSettingsRecyclerView.setAdapter(new MainMenuAdapter(getActivity(), getSettingsMenuItems()));
        mAboutRecyclerView.setAdapter(new MainMenuAdapter(getActivity(), getAboutMenuItems()));
    }

    protected ArrayList<DrawerMenuItem> getSettingsMenuItems()
    {
        ArrayList<DrawerMenuItem> menuItems = new ArrayList<>();
        menuItems.add(getLanguagesMenuItem());
        menuItems.add(getStarterPhrasesMenuItem());
        menuItems.add(getExportDataMenuItem());
        return menuItems;
    }

    @NonNull
    private DrawerMenuItem getExportDataMenuItem() {


        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, mPhraseService.getJSONExport());
        sendIntent.setType("text/plain");
        return new DrawerMenuItem(getString(R.string.export_data), sendIntent);
    }

    @NonNull
    private DrawerMenuItem getLanguagesMenuItem() {
        Intent intent = new Intent(getActivity(), LanguagesActivity_.class);
        return new DrawerMenuItem(getString(R.string.languages),intent);
    }

    @NonNull
    private DrawerMenuItem getStarterPhrasesMenuItem() {
        Intent intent = new Intent(getActivity(), AddPhrasesActivity_.class);
        return new DrawerMenuItem(getString(R.string.starter_phrases),intent);
    }

    protected ArrayList<DrawerMenuItem> getAboutMenuItems(){
        ArrayList<DrawerMenuItem> menuItems = new ArrayList<>();
        menuItems.add(getVersionMenuItem());
        menuItems.add(getAboutMenuItem());
        menuItems.add(getEmailMenuItem());
        menuItems.add(getRateusMenuItem());
        return menuItems;
    }

    @NonNull
    private DrawerMenuItem getRateusMenuItem() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getActivity().getPackageName()));
        return new DrawerMenuItem(getString(R.string.rate_us), intent);
    }

    @NonNull
    private DrawerMenuItem getAboutMenuItem() {
        Intent intent = new Intent(getActivity(), PlainWebViewActivity.class);
        intent.putExtra(PlainWebViewActivity.EXTRA_URL, "file:///android_asset/about.html");
        return new DrawerMenuItem(getString(R.string.about_how_you_say), intent);
    }

    @NonNull
    private DrawerMenuItem getVersionMenuItem() {

        String version = "";
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            version = pInfo.versionName;
        }catch (PackageManager.NameNotFoundException ex){

        }
        return new DrawerMenuItem(getString(R.string.version) + " " + version);
    }

    @NonNull
    private DrawerMenuItem getEmailMenuItem() {


        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?to=ben@rendr.io&subject=" + getString(R.string.feedback_email_subject) + "&body=" + getString(R.string.feedback_email_body));
        intent.setData(data);

        return new DrawerMenuItem(getString(R.string.feedback_support), intent);
    }

}
