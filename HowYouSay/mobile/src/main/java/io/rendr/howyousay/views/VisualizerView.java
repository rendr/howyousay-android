package io.rendr.howyousay.views;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import io.rendr.howyousay.R;

public class VisualizerView extends View {

    public static final int MAX_AMP = 20000;
    private List<Float> amplitudes; // amplitudes for line lengths
    private int width; // width of this View
    private int height; // height of this View
    private Paint primaryLinePaint;
    private Paint secondaryLinePaint;

    // constructor
    public VisualizerView(Context context, AttributeSet attrs) {
        super(context, attrs); // call superclass constructor
        primaryLinePaint = new Paint(); // create Paint for lines
        primaryLinePaint.setColor(getResources().getColor(R.color.highlight)); // set color to green
        primaryLinePaint.setStyle(Paint.Style.STROKE);
        primaryLinePaint.setStrokeWidth(4); // set stroke width

        secondaryLinePaint = new Paint();
        secondaryLinePaint.setColor(getResources().getColor(R.color.rule_color));
        secondaryLinePaint.setStyle(Paint.Style.STROKE);
        secondaryLinePaint.setStrokeWidth(2); // set stroke width

        amplitudes = new ArrayList<>();
    }

    // called when the dimensions of the View change
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        width = w; // new width of this View
        height = h; // new height of this View

    }

    // clear all amplitudes to prepare for a new visualization
    public void clear() {
        amplitudes.clear();
        invalidate();
    }

    // add the given amplitude to the amplitudes ArrayList
    public void addAmplitude(float amplitude) {
        amplitudes.add(amplitude); // add newest to the amplitudes ArrayList

        // if the power lines completely fill the VisualizerView
        trimAmplitudes();

    }

    public void addIntensity(float intensity){
        amplitudes.add(intensity * MAX_AMP);
        trimAmplitudes();
    }

    protected void trimAmplitudes() {
        while(amplitudes.size() > 3){
            amplitudes.remove(0);
        }
    }

    // draw the visualizer with scaled lines representing the amplitudes

    private ArrayList<Path> mPreviousPaths = new ArrayList<>();

    @Override
    public void onDraw(Canvas canvas) {
        float middleY = height / 2; // get the middle of the View
        float middleX = width/2;

        if(amplitudes.size() == 0) {
            canvas.drawLine(0,middleY,width,middleY, primaryLinePaint);
            return;
        }


        float stepSize = width/(amplitudes.size() * 2);

        Path rightPath = new Path();
        Path leftPath = new Path();

        int startIndex = amplitudes.size() - 1;
        float rightCurX = middleX - (stepSize/2);
        float leftCurX = middleX + (stepSize/2);
        rightPath.moveTo(rightCurX, middleY);
        leftPath.moveTo(leftCurX, middleY);

        int yDirection = -1;

        float rightControlX = 0;
        float leftControlX = 0;
        float controlY = 0;
        float amplitude = 0;
        for (int i = startIndex; i >= 0; i--) {
            amplitude = amplitudes.get(i);
            rightControlX = rightCurX + (stepSize/2);
            leftControlX = leftCurX - (stepSize/2);
            controlY = middleY + (yDirection * calculateYDelta(amplitude));
            rightCurX += stepSize;
            leftCurX -= stepSize;
            rightPath.quadTo(rightControlX, controlY, rightCurX, middleY);
            leftPath.quadTo(leftControlX, controlY, leftCurX, middleY);
            yDirection *= -1;
        }
        float spaceLeft = leftCurX;
        rightPath.quadTo(rightCurX + (spaceLeft/2), middleY + (calculateYDelta(amplitude)/2), rightCurX + spaceLeft, middleY);
        leftPath.quadTo(leftCurX - (spaceLeft/2), middleY + (calculateYDelta(amplitude)/2), leftCurX - spaceLeft, middleY);

        while (mPreviousPaths.size() > 20){
            mPreviousPaths.remove(0);
        }
        for (Path p : mPreviousPaths) {
            canvas.drawPath(p, secondaryLinePaint);
        }


        canvas.drawPath(rightPath, primaryLinePaint);
        canvas.drawPath(leftPath, primaryLinePaint);

        mPreviousPaths.add(rightPath);
        mPreviousPaths.add(leftPath);

    }

    protected float calculateYDelta(float amplitude){
        float yPercent = amplitude/ MAX_AMP;
        float halfHeight = height/2;
        return halfHeight * yPercent;
    }

}