package io.rendr.howyousay.services;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.List;

import io.rendr.howyousay.R;

/**
 * Created by ben on 12/5/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class PermissionsService {

    public static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    public static final int PERMISSIONS_REQUEST_SEED = 2;


    public static boolean hasWriteExternalStoragePermission(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasReadExternalStoragePermission(Activity  activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean hasMicPermission(Activity activity) {
        int permissionCheck = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.RECORD_AUDIO);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static void displayRecordPermissionsNeededDialog(final Activity activity) {
        new AlertDialog.Builder(activity)
                .setTitle(R.string.permissions_needed)
                .setMessage(R.string.cannot_record_audio)
                .setPositiveButton(R.string.grant_permissions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        requestRequiredRecordingPermissions(activity);
                    }
                })
                .setNegativeButton(R.string.no_thanks, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    public static void displaySeedPermissionsNeededDialog(final Activity activity){
        new AlertDialog.Builder(activity)
                .setTitle(R.string.permissions_needed)
                .setMessage(R.string.cannot_seed_phrases)
                .setPositiveButton(R.string.grant_permissions, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        requestSeedPermissions(activity);
                    }
                })
                .setNegativeButton(R.string.no_thanks, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();
    }

    public static void requestRequiredRecordingPermissions(Activity activity) {

        List<String> permissions = new ArrayList<>();

        if (!PermissionsService.hasMicPermission(activity)) {
            permissions.add(Manifest.permission.RECORD_AUDIO);
        }
        if (!PermissionsService.hasReadExternalStoragePermission(activity) || !PermissionsService.hasWriteExternalStoragePermission(activity)) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        String[] permissionsArr = new String[permissions.size()];
        permissionsArr = permissions.toArray(permissionsArr);

        ActivityCompat.requestPermissions(activity,
                permissionsArr,
                PERMISSIONS_REQUEST_RECORD_AUDIO);
    }

    public static void requestSeedPermissions(Activity activity){
        List<String> permissions = new ArrayList<>();

        if (!PermissionsService.hasWriteExternalStoragePermission(activity)) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        String[] permissionsArr = new String[permissions.size()];
        permissionsArr = permissions.toArray(permissionsArr);

        ActivityCompat.requestPermissions(activity,
                permissionsArr,
                PERMISSIONS_REQUEST_SEED);
    }
}
