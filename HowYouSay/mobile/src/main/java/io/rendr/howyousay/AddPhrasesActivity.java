package io.rendr.howyousay;

import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;

import io.rendr.howyousay.services.PermissionsService;

@EActivity(R.layout.activity_add_phrases)
public class AddPhrasesActivity extends AppCompatActivity {


    @AfterViews
    protected void initActionBar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getFragment().addPhrases();

        } else {
            PermissionsService.displaySeedPermissionsNeededDialog(this);
        }
    }

    protected AddPhrasesActivityFragment getFragment() {
        return (AddPhrasesActivityFragment)getSupportFragmentManager().findFragmentById(R.id.fragment);
    }

    @OptionsItem(android.R.id.home)
    public void goHome()
    {
        this.finish();
    }




}
