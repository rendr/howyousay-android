package io.rendr.howyousay.core.json;


import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.UUID;

import io.rendr.howyousay.core.json.PhraseJSONParser;
import io.rendr.howyousay.core.json.PhraseTranslationParser;
import io.rendr.howyousay.core.models.Phrase;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.*;
/**
 * Created by ben on 12/25/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class PhraseJsonParseTest {

    @Test
    public void givenJSONSObject_returnsPhrase() throws JSONException{
        //Arrange
        JSONObject mockedJSONObj = mock(JSONObject.class);
        String expectedTitle = stubTitle(mockedJSONObj);
        stubIsBookmarked(mockedJSONObj);
        int expectedNumberOfTranslations = stubTranslationArray(mockedJSONObj);
        PhraseTranslationParser mockedTranslationParser = mock(PhraseTranslationParser.class);

        //Act
        Phrase parsedModel = new PhraseJSONParser().parse(mockedJSONObj, mockedTranslationParser);

        //Assert
        assertEquals(expectedTitle, parsedModel.getTitle());
        assertTrue(parsedModel.isBookmarked());
        assertEquals(expectedNumberOfTranslations, parsedModel.getTranslations().size());
        verify(mockedTranslationParser, times(2)).parse(any(JSONObject.class),any(UUID.class));

    }

    protected int stubTranslationArray(JSONObject mockedJSONObj) throws JSONException {
        JSONArray mockTranslationJSONArray = mock(JSONArray.class);
        when(mockedJSONObj.getJSONArray("translations")).thenReturn(mockTranslationJSONArray);
        int expectedNumberOfTranslations = 2;
        when(mockTranslationJSONArray.length()).thenReturn(expectedNumberOfTranslations);
        return expectedNumberOfTranslations;
    }

    private Boolean stubIsBookmarked(JSONObject mockedJSONObj) throws JSONException{
        boolean expectedValue = true;
        when(mockedJSONObj.getBoolean("isBookmarked")).thenReturn(expectedValue);
        return expectedValue;
    }

    @NonNull
    protected String stubTitle(JSONObject mockedJSONObj) throws JSONException {
        String expectedTitle = "Where is the bathroom?";
        when(mockedJSONObj.getString("title")).thenReturn(expectedTitle);
        return expectedTitle;
    }

}
