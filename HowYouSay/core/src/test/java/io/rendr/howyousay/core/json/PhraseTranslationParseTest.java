package io.rendr.howyousay.core.json;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.UUID;

import io.rendr.howyousay.core.json.PhraseTranslationParser;
import io.rendr.howyousay.core.models.PhraseTranslation;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by ben on 12/25/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class PhraseTranslationParseTest {

    @Test
    public void givenJSONSObject_returnsPhraseTranslation() throws JSONException
    {
        //Arrange
        PhraseTranslationParser parserUT = new PhraseTranslationParser();
        JSONObject mockJSONObj = mock(JSONObject.class);
        String expectedContent = stubContent(mockJSONObj);
        String expectedNotes = stubNotes(mockJSONObj);
        String expectedLanguage = stubLanguage(mockJSONObj);
        String expectedPhonetic = stubPhonetic(mockJSONObj);
        UUID expectedPhraseUUID = UUID.randomUUID();

        //Act
        PhraseTranslation model = parserUT.parse(mockJSONObj, expectedPhraseUUID);

        //Assert
        assertEquals(expectedContent, model.getTitle());
        assertEquals(expectedNotes, model.getNotes());
        assertEquals(expectedLanguage, model.getLanguageTitle());
        assertEquals(expectedPhonetic, model.getPhonetic());
        assertEquals(expectedPhraseUUID, model.getPhraseUUID());
    }

    @NonNull
    protected String stubPhonetic(JSONObject mockJSONObj) throws JSONException {
        String expectedPhonetic = "How-Dee";
        when(mockJSONObj.getString("phonetic")).thenReturn(expectedPhonetic);
        return expectedPhonetic;
    }

    @NonNull
    protected String stubLanguage(JSONObject mockJSONObj) throws JSONException {
        String expectedLanguage = "Albanian";
        when(mockJSONObj.getString("language")).thenReturn(expectedLanguage);
        return expectedLanguage;
    }

    @NonNull
    protected String stubNotes(JSONObject mockJSONObj) throws JSONException {
        String expectedNotes = "Albania. Source: Valentina Petraj from Gjakova, Kosova";
        when(mockJSONObj.getString("notes")).thenReturn(expectedNotes);
        return expectedNotes;
    }

    @NonNull
    protected String stubContent(JSONObject mockJSONObj) throws JSONException {
        String expectedContent = "Si I thoni?";
        when(mockJSONObj.getString("content")).thenReturn(expectedContent);
        return expectedContent;
    }


}
