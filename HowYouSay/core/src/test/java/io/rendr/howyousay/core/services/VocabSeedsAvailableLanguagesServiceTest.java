package io.rendr.howyousay.core.services;

import android.app.Activity;
import android.support.annotation.NonNull;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import io.rendr.howyousay.core.services.AssetsService;
import io.rendr.howyousay.core.services.VocabSeedsAvailableLanguagesService;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by ben on 12/26/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class VocabSeedsAvailableLanguagesServiceTest {

    @Test
    public void givenAssetsService_returnsListOfLanguages() throws IOException {
        //Arrange
        AssetsService mockedAssetsService = mock(AssetsService.class);
        Activity mockedActivity = mock(Activity.class);
        when(mockedAssetsService.getJSONFiles(mockedActivity)).thenReturn(getJSONFiles());

        //Act
        ArrayList languages = new VocabSeedsAvailableLanguagesService().get(mockedAssetsService, mockedActivity);

        //Assert
        assertEquals(getExpectedLanguages(), languages);

    }

    @NonNull
    protected ArrayList<String> getExpectedLanguages() {
        ArrayList<String> expectedLanguages = new ArrayList<String>();
        expectedLanguages.add("Albanian");
        expectedLanguages.add("English");
        expectedLanguages.add("Spanish");
        expectedLanguages.add("German");
        return expectedLanguages;
    }

    private ArrayList<String> getJSONFiles() {
        ArrayList<String> fileNames = new ArrayList<>();
        fileNames.add("Albanian.json");
        fileNames.add("english.json");
        fileNames.add("Spanish.json");
        fileNames.add("German.json");
        return fileNames;
    }
}
