package io.rendr.howyousay.core.services;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;

import io.rendr.howyousay.core.json.PhraseJSONParser;
import io.rendr.howyousay.core.json.PhraseTranslationParser;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ben on 12/26/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class VocabSeedsAddPhrasesServiceTest {

    @Test
    public void givenServices_shouldLoadJSONFiles() {
        //Arrange
        VocabSeedsAddPhrasesService serviceUT = new VocabSeedsAddPhrasesService();

        JSONFileService mockedJSONFileService = mock(JSONFileService.class);
        serviceUT.mJSONFileService = mockedJSONFileService;

        PhraseTranslationParser mockedTranslationParser = mock(PhraseTranslationParser.class);
        serviceUT.mPhraseTranslationParser = mockedTranslationParser;

        JSONObject spanishJSONObj = getJSONObject("Spanish.json", mockedJSONFileService);
        JSONObject germanJSONObj = getJSONObject("German.json", mockedJSONFileService);
        JSONObject japaneseJSONObj = getJSONObject("Japanese.json", mockedJSONFileService);

        PhraseJSONParser mockedPhraseJSONParser = mock(PhraseJSONParser.class);
        serviceUT.mPhraseJSONParser = mockedPhraseJSONParser;

        //Act
        serviceUT.add(getSelectedLanguages());


        //Assert
        try {
            verify(mockedPhraseJSONParser, times(1)).parse(spanishJSONObj, mockedTranslationParser);
            verify(mockedPhraseJSONParser, times(1)).parse(germanJSONObj, mockedTranslationParser);
            verify(mockedPhraseJSONParser, times(1)).parse(japaneseJSONObj, mockedTranslationParser);
        } catch (JSONException ex) {

        }


    }

    @NonNull
    protected JSONObject getJSONObject(String language, JSONFileService mockedJSONFileService) {
        JSONObject jsonObj = mock(JSONObject.class);
        when(mockedJSONFileService.getJSONObject(language)).thenReturn(jsonObj);
        return jsonObj;
    }


    @NonNull
    protected ArrayList<String> getSelectedLanguages() {
        ArrayList<String> languages = new ArrayList<>();
        languages.add("Spanish");
        languages.add("German");
        languages.add("Japanese");
        return languages;
    }


}
