package io.rendr.howyousay.core.services;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.util.Log;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ben on 12/25/15.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean
public class JSONFileService {

    @RootContext
    public Activity activity;

    public JSONObject getJSONObject(String fileName)
    {
        String jsonStr = readFileText(fileName);
        return parseJSONText(fileName, jsonStr);
    }

    @Nullable
    protected JSONObject parseJSONText(String fileName, String jsonStr) {
        try {
            return new JSONObject(jsonStr);
        }catch (JSONException jex){
            Log.w("JSONException", "Error trying to parse json file: " + fileName);
        }
        return null;
    }

    protected String readFileText(String fileName) {
        String jsonStr = "";
        try {
            InputStream is = activity.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonStr = new String(buffer, "UTF-8");
        }catch (IOException ex){
            Log.w("IOException", "Error trying to read json file: " + fileName);
        }
        return jsonStr;
    }
}
