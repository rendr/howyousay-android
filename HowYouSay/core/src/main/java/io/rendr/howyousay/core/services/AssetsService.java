package io.rendr.howyousay.core.services;

import android.app.Activity;
import android.content.res.AssetManager;


import org.androidannotations.annotations.EBean;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ben on 12/25/15.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean
public class AssetsService {

    public ArrayList<String> getJSONFiles(Activity activity) throws IOException
    {

        String[] fileNames = activity.getAssets().list("");
        ArrayList<String> jsonFileNames = new ArrayList<>();
        for (String fileName : fileNames) {
            if(fileName.endsWith(".json"))
                jsonFileNames.add(fileName);
        }

        return jsonFileNames;
    }

}
