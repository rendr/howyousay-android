package io.rendr.howyousay.core.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by ben on 11/19/15.
 */
public class PhraseBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "phraseBase.db";

    private static final int VERSION = 1;

    public PhraseBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + PhraseDbSchema.PhraseTable.NAME + " (" +
                " _id integer primary key autoincrement, " +
                PhraseDbSchema.PhraseTable.Cols.UUID + ", " +
                PhraseDbSchema.PhraseTable.Cols.Title + ", " +
                PhraseDbSchema.PhraseTable.Cols.Bookmarked +
                ")");

        db.execSQL("create table " + PhraseDbSchema.PhraseTranslationTable.NAME + " (" +
                " _id integer primary key autoincrement, " +
                PhraseDbSchema.PhraseTranslationTable.Cols.UUID + ", " +
                PhraseDbSchema.PhraseTranslationTable.Cols.Phrase_UUID + ", " +
                PhraseDbSchema.PhraseTranslationTable.Cols.Title + ", " +
                PhraseDbSchema.PhraseTranslationTable.Cols.Phonetic + ", " +
                PhraseDbSchema.PhraseTranslationTable.Cols.LanguageTitle + ", " +
                PhraseDbSchema.PhraseTranslationTable.Cols.Tags + ", " +
                PhraseDbSchema.PhraseTranslationTable.Cols.Notes+
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
