package io.rendr.howyousay.core.services;

import android.os.Environment;
import android.util.Log;

import org.androidannotations.annotations.EBean;

import java.io.File;

/**
 * Created by ben on 1/8/16.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean
public class AudioFileCleanUpService {

    public void cleanUp()
    {
        File dirToCleanUp = new File(Environment.getExternalStorageDirectory(), "howyousay");

        File[] file = dirToCleanUp.listFiles();
        for (File f : file) {
            String fileName = f.getName();
            String[] fileNameParts = fileName.split("-");
            if(fileNameParts.length > 4 && fileName.endsWith(".m4a")){
                f.delete();

            }
        }

    }
}
