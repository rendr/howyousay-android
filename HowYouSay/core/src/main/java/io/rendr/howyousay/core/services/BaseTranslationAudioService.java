package io.rendr.howyousay.core.services;

import android.os.Environment;

import java.util.UUID;

/**
 * Created by ben on 12/5/15.
 * Copyright Rendr LLC All Rights Reserved
 */

public abstract class BaseTranslationAudioService {
    protected final UUID mTranslationUUID;

    public BaseTranslationAudioService(UUID translationUUID) {
        mTranslationUUID = translationUUID;
    }

    protected String getAudioFilePath() {
        return FilePathService.getTranslationAudioPath(mTranslationUUID);
    }
}
