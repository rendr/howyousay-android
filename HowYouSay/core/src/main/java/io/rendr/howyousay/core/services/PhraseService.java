package io.rendr.howyousay.core.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.rendr.howyousay.core.database.PhraseBaseHelper;
import io.rendr.howyousay.core.database.PhraseCursorWrapper;
import io.rendr.howyousay.core.database.PhraseDbSchema;
import io.rendr.howyousay.core.json.PhraseJSONWrapper;
import io.rendr.howyousay.core.models.Phrase;
import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 11/19/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PhraseService {

    private static PhraseService sPhraseService;

    @Bean
    LanguagePreferencesService mLanguagePreferencesService;

    @RootContext
    public Context mContext;

    private SQLiteDatabase mDatabase;

    @AfterInject
    public void init() {
        mDatabase = new PhraseBaseHelper(mContext).getWritableDatabase();
    }

    public void savePhrase(Phrase phrase) {
        checkNoMediaFile();
        if (tableRowExists(PhraseDbSchema.PhraseTable.NAME, phrase.getUUID())) {
            updatePhrase(phrase);
        } else {
            addPhrase(phrase);
        }
        enablePhraseLanguages(phrase);
    }

    private void checkNoMediaFile() {
        File baseDirectory = new File(Environment.getExternalStorageDirectory(), "howyousay");
        File nomediaFile = new File(baseDirectory, ".nomedia");
        try {
            nomediaFile.createNewFile();
        }catch (IOException ex){
            Log.w("IO Exception", "Error creating nomedia file: " + ex.getMessage());
        }
    }


    public Phrase getPhrase(UUID id) {
        PhraseCursorWrapper cursor = queryTable(PhraseDbSchema.PhraseTable.NAME, PhraseDbSchema.PhraseTable.Cols.UUID + " = ?", new String[]{id.toString()});

        if (!tableRowExists(PhraseDbSchema.PhraseTable.NAME, id))
            return null;

        cursor.moveToFirst();
        Phrase phrase = cursor.getPhrase();
        PopulatePhraseTranslations(phrase);
        return phrase;
    }

    public void deletePhrase(UUID id) {

        mDatabase.delete(PhraseDbSchema.PhraseTable.NAME, PhraseDbSchema.PhraseTable.Cols.UUID + " = ?", new String[]{id.toString()});
        mDatabase.delete(PhraseDbSchema.PhraseTranslationTable.NAME, PhraseDbSchema.PhraseTranslationTable.Cols.Phrase_UUID + " = ?", new String[]{id.toString()});
    }

    public void deleteTranslation(UUID id) {
        mDatabase.delete(PhraseDbSchema.PhraseTranslationTable.NAME, PhraseDbSchema.PhraseTranslationTable.Cols.UUID + " = ?", new String[]{id.toString()});
    }

    public PhraseTranslation getPhraseTranslation(UUID id) {
        PhraseCursorWrapper cursor = queryTable(PhraseDbSchema.PhraseTranslationTable.NAME, PhraseDbSchema.PhraseTranslationTable.Cols.UUID + " = ?", new String[]{id.toString()});

        if (!tableRowExists(PhraseDbSchema.PhraseTranslationTable.NAME, id))
            return null;

        cursor.moveToFirst();
        return cursor.getTranslation();
    }

    public boolean phraseTranslationExists(String translationTitle, UUID phraseUUID) {
        PhraseCursorWrapper cursor = queryTable(PhraseDbSchema.PhraseTranslationTable.NAME, PhraseDbSchema.PhraseTranslationTable.Cols.Phrase_UUID + " = ? AND " + PhraseDbSchema.PhraseTranslationTable.Cols.Title + " = ?", new String[]{phraseUUID.toString(), translationTitle});
        int matchingCount = cursor.getCount();
        cursor.close();
        return matchingCount > 0;
    }

    public List<Phrase> getPhrases(String searchCriteria) {
        return getPhrasesFromDB(getSearchClause(searchCriteria), null);
    }


    public List<Phrase> getBookmarkedPhrases(String searchCriteria) {
        return getPhrasesFromDB(getSearchClause(searchCriteria) + " AND " + PhraseDbSchema.PhraseTable.Cols.Bookmarked + " = 1", null);
    }

    public boolean phraseExists(String phrase) {

        PhraseCursorWrapper cursor = queryTable(PhraseDbSchema.PhraseTable.NAME, PhraseDbSchema.PhraseTable.Cols.Title + " = ?", new String[]{phrase});

        int matchCount = cursor.getCount();
        cursor.close();
        return matchCount > 0;

    }

    public Phrase getPhraseByTitle(String title) {
        PhraseCursorWrapper cursor = queryTable(PhraseDbSchema.PhraseTable.NAME, PhraseDbSchema.PhraseTable.Cols.Title + " = ?", new String[]{title});
        cursor.moveToFirst();
        return cursor.getPhrase();
    }

    public ArrayList<String> getLanguages() {

        String queryString = "SELECT DISTINCT lower(" + PhraseDbSchema.PhraseTranslationTable.Cols.LanguageTitle + ") as title FROM " + PhraseDbSchema.PhraseTranslationTable.NAME + " ORDER BY title";

        Cursor cursor = mDatabase.rawQuery(queryString, new String[]{});
        ArrayList<String> languages = new ArrayList<>();

        if (cursor.getCount() == 0) {
            return languages;
        }
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            try {
                String language = cursor.getString(0);
                languages.add(language.substring(0, 1).toUpperCase() + language.substring(1));
            } catch (Exception ex) {
                Log.w("SQL Issue", "Issue getting languages.");
            }
            cursor.moveToNext();
        }
        return languages;
    }

    @NonNull
    protected String getSearchClause(String searchCriteria) {
        return PhraseDbSchema.PhraseTable.Cols.Title + " LIKE '%" + searchCriteria + "%'";
    }

    protected List<Phrase> getPhrasesFromDB(String whereClause, String[] clauseArgs) {
        List<Phrase> phrases = new ArrayList<>();
        PhraseCursorWrapper cursor = queryTable(PhraseDbSchema.PhraseTable.NAME, whereClause, clauseArgs, "lower("+PhraseDbSchema.PhraseTable.Cols.Title+")");
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Phrase phrase = cursor.getPhrase();
                PopulatePhraseTranslations(phrase);
                if (phrase.getTranslations().size() > 0)
                    phrases.add(phrase);
                cursor.moveToNext();
            }

        } finally {
            cursor.close();
        }

        return phrases;
    }

    private void PopulatePhraseTranslations(Phrase phrase) {
        List<PhraseTranslation> translations = getPhraseTranslations(phrase.getUUID());
        for (PhraseTranslation translation : translations) {
            if (!mLanguagePreferencesService.isLanguageDisabled(translation.getLanguageTitle())) {
                phrase.getTranslations().add(translation);
            }
        }
    }

    protected List<PhraseTranslation> getPhraseTranslations(UUID phraseId) {
        List<PhraseTranslation> translations = new ArrayList<>();
        PhraseCursorWrapper cursor = queryTable(PhraseDbSchema.PhraseTranslationTable.NAME, PhraseDbSchema.PhraseTranslationTable.Cols.Phrase_UUID + " = ?", new String[]{phraseId.toString()});

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                PhraseTranslation translation = cursor.getTranslation();

                translations.add(translation);
                cursor.moveToNext();
            }

        } finally {
            cursor.close();
        }

        return translations;
    }


    protected PhraseCursorWrapper queryTable(String tableName, String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                tableName,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );
        return new PhraseCursorWrapper(cursor);
    }

    protected PhraseCursorWrapper queryTable(String tableName, String whereClause, String[] whereArgs, String orderBy) {
        Cursor cursor = mDatabase.query(
                tableName,
                null,
                whereClause,
                whereArgs,
                null,
                null,
                orderBy
        );
        return new PhraseCursorWrapper(cursor);
    }

    protected void addPhrase(Phrase phrase) {

        mDatabase.insert(PhraseDbSchema.PhraseTable.NAME, null, getContentValues(phrase));
        phrase.TrimTranslations();
        for (PhraseTranslation translation : phrase.getTranslations()) {
            addPhraseTranslation(translation);
        }

    }

    protected void updatePhrase(Phrase phrase) {
        mDatabase.update(PhraseDbSchema.PhraseTable.NAME, getContentValues(phrase), PhraseDbSchema.PhraseTable.Cols.UUID + " = ?", new String[]{phrase.getUUID().toString()});
        phrase.TrimTranslations();
        for (PhraseTranslation translation : phrase.getTranslations()) {
            if (tableRowExists(PhraseDbSchema.PhraseTranslationTable.NAME, translation.getUUID())) {
                updatePhraseTranslation(translation);
            } else {
                addPhraseTranslation(translation);
            }
        }
    }

    protected void updatePhraseTranslation(PhraseTranslation translation) {
        mDatabase.update(
                PhraseDbSchema.PhraseTranslationTable.NAME,
                getContentValues(translation),
                PhraseDbSchema.PhraseTranslationTable.Cols.UUID + " = ?", new String[]{translation.getUUID().toString()});
    }

    protected void addPhraseTranslation(PhraseTranslation translation) {
        mDatabase.insert(PhraseDbSchema.PhraseTranslationTable.NAME, null, getContentValues(translation));
    }

    protected void enablePhraseLanguages(Phrase phrase) {
        for (PhraseTranslation translation : phrase.getTranslations()) {
            mLanguagePreferencesService.setLanguageDisabled(translation.getLanguageTitle(), false);
        }

    }


    protected boolean tableRowExists(String tableName, UUID uuid) {

        PhraseCursorWrapper cursor = queryTable(tableName, "uuid = ?", new String[]{uuid.toString()});

        int count = cursor.getCount();
        if (count <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }


    private static ContentValues getContentValues(Phrase phrase) {
        ContentValues values = new ContentValues();
        values.put(PhraseDbSchema.PhraseTable.Cols.UUID, phrase.getUUID().toString());
        values.put(PhraseDbSchema.PhraseTable.Cols.Title, phrase.getTitle());
        values.put(PhraseDbSchema.PhraseTable.Cols.Bookmarked, phrase.isBookmarked());
        return values;
    }

    private static ContentValues getContentValues(PhraseTranslation translation) {
        ContentValues values = new ContentValues();
        values.put(PhraseDbSchema.PhraseTranslationTable.Cols.UUID, translation.getUUID().toString());
        values.put(PhraseDbSchema.PhraseTranslationTable.Cols.Phrase_UUID, translation.getPhraseUUID().toString());
        values.put(PhraseDbSchema.PhraseTranslationTable.Cols.Title, translation.getTitle());
        values.put(PhraseDbSchema.PhraseTranslationTable.Cols.Phonetic, translation.getPhonetic());
        values.put(PhraseDbSchema.PhraseTranslationTable.Cols.LanguageTitle, translation.getLanguageTitle());
        values.put(PhraseDbSchema.PhraseTranslationTable.Cols.Tags, translation.getTags());
        values.put(PhraseDbSchema.PhraseTranslationTable.Cols.Notes, translation.getNotes());
        return values;
    }


    public String getJSONExport() {
        JSONArray jsonArray = new JSONArray();
        List<Phrase> phrases = this.getPhrases("");
        try {
            for (int i = 0; i < phrases.size(); i++) {
                Phrase phrase = phrases.get(i);
                jsonArray.put(i, new PhraseJSONWrapper(phrase));
            }
        } catch (JSONException ex) {

        }

        return jsonArray.toString();
    }
}
