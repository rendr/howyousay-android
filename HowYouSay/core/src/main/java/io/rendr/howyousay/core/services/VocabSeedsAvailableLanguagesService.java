package io.rendr.howyousay.core.services;

import android.app.Activity;

import org.androidannotations.annotations.EBean;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ben on 12/25/15.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean
public class VocabSeedsAvailableLanguagesService {



    public ArrayList<String> get(AssetsService assetsService, Activity activity) throws IOException
    {
        ArrayList<String> jsonFileNames = assetsService.getJSONFiles(activity);
        ArrayList<String> languages = new ArrayList<>();
        for (String fileName : jsonFileNames) {

            languages.add((Character.toUpperCase(fileName.charAt(0)) + fileName.substring(1).replace(".json", "")));
        }


        return languages;
    }
}
