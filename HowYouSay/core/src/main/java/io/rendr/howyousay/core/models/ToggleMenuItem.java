package io.rendr.howyousay.core.models;

/**
 * Created by ben on 12/28/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class ToggleMenuItem {

    String mTitle;
    public String getTitle() {
        return mTitle;
    }

    Boolean mSelected = false;

    public Boolean getSelected() {
        return mSelected;
    }

    public void setSelected(Boolean selected) {
        mSelected = selected;
    }

    public ToggleMenuItem(String title) {
        mTitle = title;
    }

    public ToggleMenuItem(String title, Boolean selected) {
        mTitle = title;
        mSelected = selected;
    }
}
