package io.rendr.howyousay.core.json;

import org.json.JSONException;
import org.json.JSONObject;

import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 12/31/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class PhraseTranslationJSONWrapper extends JSONObject {
    public PhraseTranslationJSONWrapper(PhraseTranslation translation) throws JSONException{
        put("title", translation.getTitle());
        put("language", translation.getLanguageTitle());
        put("notes", translation.getNotes());
        put("phonetic", translation.getPhonetic());

    }
}
