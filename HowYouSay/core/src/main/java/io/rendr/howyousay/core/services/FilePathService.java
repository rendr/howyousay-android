package io.rendr.howyousay.core.services;

import android.os.Environment;

import java.io.File;
import java.util.UUID;

/**
 * Created by ben on 1/8/16.
 * Copyright Rendr LLC All Rights Reserved
 */
public class FilePathService {

    public static String getTranslationAudioPath(UUID translationUUID)
    {
        String mAudioFilePath = Environment.getExternalStorageDirectory().getAbsolutePath();

        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/io.rendr.howyousay/");
        if(!dir.exists()) {
            dir.mkdir();
        }
        mAudioFilePath += "/io.rendr.howyousay/" + translationUUID.toString() + ".m4a";
        return mAudioFilePath;
    }
}
