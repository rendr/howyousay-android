package io.rendr.howyousay.core.json;


import org.androidannotations.annotations.EBean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

import io.rendr.howyousay.core.models.Phrase;
import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 12/22/15.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean
public class PhraseJSONParser {



    public Phrase parse(JSONObject jsonObject, PhraseTranslationParser translationParser) throws JSONException {

        UUID phraseID = UUID.randomUUID();
        Phrase phrase = new Phrase(phraseID, new ArrayList<PhraseTranslation>());
        phrase.setTitle(jsonObject.getString("title"));
        phrase.setBookmarked(jsonObject.getBoolean("isBookmarked"));


        JSONArray jsonArray = jsonObject.getJSONArray("translations");
        for (int i = 0; i < jsonArray.length(); i++) {
            phrase.getTranslations().add(translationParser.parse(jsonArray.getJSONObject(i),phraseID));
        }


        return phrase;
    }


}
