package io.rendr.howyousay.core.models;

import java.io.Serializable;
import java.util.UUID;

import io.rendr.howyousay.core.util.StringUtil;

/**
 * Created by ben on 11/19/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class PhraseTranslation implements Serializable {


    private UUID mUUID;

    public UUID getUUID() {
        return mUUID;
    }


    private UUID mPhraseUUID;

    public UUID getPhraseUUID() {
        return mPhraseUUID;
    }

    public void setPhraseUUID(UUID phraseUUID) {
        mPhraseUUID = phraseUUID;
    }

    private String mTitle;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    private String mPhonetic;

    public String getPhonetic() {
        return mPhonetic;
    }

    public void setPhonetic(String phonetic) {
        mPhonetic = phonetic;
    }

    private int mLanguageId;


    public int getLanguageId() {
        return mLanguageId;
    }

    private String mLanguageTitle;


    public String getLanguageTitle() {
        return mLanguageTitle;
    }

    public void setLanguageTitle(String languageTitle) {
        mLanguageTitle = languageTitle;
    }

    private String mTags;

    public String getTags() {
        return mTags;
    }

    public void setTags(String mTags) {
        this.mTags = mTags;
    }

    private String mNotes;

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String mNotes) {
        this.mNotes = mNotes;
    }

    public PhraseTranslation(UUID UUID, UUID phraseUUID) {
        mUUID = UUID;
        mPhraseUUID = phraseUUID;
    }

    public boolean isEmpty()
    {
        return StringUtil.isNullOrEmpty(mTitle) && StringUtil.isNullOrEmpty(mLanguageTitle) && StringUtil.isNullOrEmpty(mNotes) && StringUtil.isNullOrEmpty(mPhonetic);
    }
}
