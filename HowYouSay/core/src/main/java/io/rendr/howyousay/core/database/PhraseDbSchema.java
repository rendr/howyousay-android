package io.rendr.howyousay.core.database;

/**
 * Created by ben on 11/19/15.
 */
public class PhraseDbSchema {
    public static final class PhraseTable{
        public static final String NAME = "phrases";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String Title = "title";
            public static final String Bookmarked = "bookmarked";

        }
    }

    public static final class PhraseTranslationTable{
        public static final String NAME = "phrase_translations";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String Phrase_UUID = "phrase_uuid";
            public static final String Title = "title";
            public static final String Phonetic= "phonetic";
            public static final String LanguageTitle = "language_title";
            public static final String Tags = "tags";
            public static final String Notes = "notes";

        }
    }

}
