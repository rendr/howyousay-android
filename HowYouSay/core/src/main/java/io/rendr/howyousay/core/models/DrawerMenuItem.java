package io.rendr.howyousay.core.models;

import android.content.Intent;

/**
 * Created by ben on 11/15/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class DrawerMenuItem {

    Intent mIntent;
    public Intent getIntent(){
        return mIntent;
    }

    String mTitle;
    public String getTitle() {
        return mTitle;
    }



    public DrawerMenuItem(String title) {
        mTitle = title;

    }

    public DrawerMenuItem(String title, Intent intent) {
        mTitle = title;
        mIntent = intent;
    }
}
