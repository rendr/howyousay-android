package io.rendr.howyousay.core.models;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by ben on 11/15/15.
 */
public class Phrase implements Serializable{


    private UUID mUUID;

    public UUID getUUID() {
        return mUUID;
    }

    private String mTitle;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    private boolean mBookmarked;

    public boolean isBookmarked() {
        return mBookmarked;
    }

    public void setBookmarked(boolean bookmarked) {
        mBookmarked = bookmarked;
    }


    private List<PhraseTranslation> mTranslations;

    public String getTranslationTitle()
    {
        List<String> titles = new ArrayList<>();

        for (PhraseTranslation translation : mTranslations) {
            titles.add(translation.getTitle());
        }
        return TextUtils.join(", ", titles);


    }

    public List<PhraseTranslation> getTranslations() {
        return mTranslations;
    }


    public void TrimTranslations()
    {
        List<PhraseTranslation> translations = getTranslations();
        for (int i = 0; i < translations.size(); i++) {
            PhraseTranslation translation = translations.get(i);
            if (translation.isEmpty() && getTranslations().size() > 1) {
                getTranslations().remove(translation);
                i--;
            }
        }

    }

    public Phrase(UUID uuid, List<PhraseTranslation> translations) {

        mUUID = uuid;
        mTranslations = translations;

    }
}
