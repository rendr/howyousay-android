package io.rendr.howyousay.core.util;

/**
 * Created by ben on 12/31/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class StringUtil {
    public static boolean isNullOrEmpty(String string){
        boolean isEmpty = string == null || string.trim().length() == 0;
        return isEmpty;
    }
}
