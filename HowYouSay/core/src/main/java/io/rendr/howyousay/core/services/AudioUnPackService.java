package io.rendr.howyousay.core.services;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 12/29/15.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean
public class AudioUnPackService {

    @RootContext
    public Context mContext;

    public void unpackTranslationAudio(UUID translationUUID, String language, int phraseIndex, int translationIndex)
    {
        try {
            InputStream assetStream = mContext.getAssets().open(getTranslationAssetsAudioFileName(language, phraseIndex, translationIndex));
            String destPath = getTranslationDestinationAudioFilePath(translationUUID);
            File outFile = new File(destPath);
            OutputStream destinationStream = new FileOutputStream(outFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = assetStream.read(buf)) > 0) {
                destinationStream.write(buf, 0, len);
            }
            assetStream.close();
            destinationStream.close();
        }catch (IOException ex){
            Log.w("IOException", "Exception encountered trying to copy translation audio");
        }

    }

    protected String getTranslationDestinationAudioFilePath(UUID translationUUID){
        return FilePathService.getTranslationAudioPath(translationUUID);

    }

    protected String getTranslationAssetsAudioFileName(String language, int phraseIndex, int translationIndex) {
        phraseIndex = phraseIndex + 1;
        translationIndex = translationIndex + 1;
        String phraseIndexLabel = ("000" + phraseIndex).substring(String.valueOf(phraseIndex).length());
        String translationIndexLabel = ("000" + translationIndex).substring(String.valueOf(translationIndex).length());


        String languagePrefix = language.toUpperCase();
        if(languagePrefix.contains(" "))
            languagePrefix = languagePrefix.split(" ")[0];
        return languagePrefix + "-" + phraseIndexLabel + "-" + translationIndexLabel + ".m4a";
    }

}
