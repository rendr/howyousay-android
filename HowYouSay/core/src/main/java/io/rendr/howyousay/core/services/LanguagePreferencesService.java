package io.rendr.howyousay.core.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by ben on 12/30/15.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean(scope = EBean.Scope.Singleton)
public class LanguagePreferencesService {

    private SharedPreferences mSettings;

    @RootContext
    public Context mContext;

    static String PREF_DISABLED_LANGUAGES = "prefDisabledLanguages";

    @AfterInject
    public void init()
    {
        mSettings = mContext.getSharedPreferences(PREF_DISABLED_LANGUAGES, 0);
    }

    public boolean isLanguageDisabled(String language)
    {
        return mSettings.getBoolean(getKey(language), false);
    }

    @NonNull
    protected String getKey(String language) {
        return language.toLowerCase() + "Disabled";
    }

    public void setLanguageDisabled(String language, boolean isDisabled)
    {
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putBoolean(getKey(language), isDisabled);
        editor.commit();
    }
}
