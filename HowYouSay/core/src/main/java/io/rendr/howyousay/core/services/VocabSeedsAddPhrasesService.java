package io.rendr.howyousay.core.services;

import android.os.Environment;
import android.util.Log;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.rendr.howyousay.core.json.PhraseJSONParser;
import io.rendr.howyousay.core.json.PhraseTranslationParser;
import io.rendr.howyousay.core.models.Phrase;
import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 12/26/15.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean
public class VocabSeedsAddPhrasesService {

    @Bean
    public JSONFileService mJSONFileService;

    @Bean
    public PhraseJSONParser mPhraseJSONParser;

    @Bean
    public PhraseTranslationParser mPhraseTranslationParser;

    @Bean
    public PhraseService mPhraseService;

//    @Bean
//    public AudioFileCleanUpService mAudioFileCleanUpService;

    @Bean
    public AudioUnPackService mAudioUnPackService;

    public void add(ArrayList<String> languages) {
//        mAudioFileCleanUpService.cleanUp();

        for (String language : languages) {
            String languageFileName = language + ".json";
            try {
                addLanguage(mJSONFileService.getJSONObject(languageFileName));
            } catch (JSONException jex) {
                Log.w("JSONException", "Error trying to parse: " + languageFileName);
            }
        }

    }

    protected void addLanguage(JSONObject jObj) throws JSONException {

        JSONArray jArray = jObj.getJSONArray("vocab");
        for (int i = 0; i < jArray.length(); i++) {
            addPhrase(mPhraseJSONParser.parse(jArray.getJSONObject(i), mPhraseTranslationParser), i);
        }

    }

    private void addPhrase(Phrase phraseFromJSON, int phraseIndex) throws JSONException {
        if (!phraseAlreadyInDB(phraseFromJSON)) {
            mPhraseService.savePhrase(phraseFromJSON);
            copyPhraseAudio(phraseFromJSON, phraseIndex);
        } else {
            addTranslationToExistingPhrase(mPhraseService.getPhraseByTitle(phraseFromJSON.getTitle()), phraseFromJSON, phraseIndex);
        }
    }

    private void copyPhraseAudio(Phrase phraseFromJSON, int phraseIndex) {
        java.util.List<PhraseTranslation> translations = phraseFromJSON.getTranslations();
        for (int i = 0; i < translations.size(); i++) {
            PhraseTranslation translation = translations.get(i);
            mAudioUnPackService.unpackTranslationAudio(translation.getUUID(), translation.getLanguageTitle(), phraseIndex, i);
        }

    }

    private void addTranslationToExistingPhrase(Phrase phraseInDB, Phrase phraseFromJSON, int phraseIndex) {

        java.util.List<PhraseTranslation> translations = phraseFromJSON.getTranslations();
        for (int i = 0; i < translations.size(); i++) {
            PhraseTranslation translation = translations.get(i);
            addTranslationIfNotInDB(phraseInDB, translation);
            mAudioUnPackService.unpackTranslationAudio(translation.getUUID(), translation.getLanguageTitle(), phraseIndex, i);
        }
    }

    private void addTranslationIfNotInDB(Phrase phraseInDB, PhraseTranslation translation) {
        if (!translationAlreadyInDB(phraseInDB, translation)) {
            translation.setPhraseUUID(phraseInDB.getUUID());
            mPhraseService.addPhraseTranslation(translation);
        }
    }

    private boolean translationAlreadyInDB(Phrase phraseInDB, PhraseTranslation translation) {
        return mPhraseService.phraseTranslationExists(translation.getTitle(), phraseInDB.getUUID());
    }

    private boolean phraseAlreadyInDB(Phrase phrase) {
        return mPhraseService.phraseExists(phrase.getTitle());
    }


}
