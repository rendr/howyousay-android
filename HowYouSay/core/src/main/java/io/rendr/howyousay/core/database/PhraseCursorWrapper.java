package io.rendr.howyousay.core.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import java.util.ArrayList;
import java.util.UUID;

import io.rendr.howyousay.core.models.Phrase;
import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 11/19/15.
 */
public class PhraseCursorWrapper extends CursorWrapper {
    /**
     * Creates a cursor wrapper.
     *
     * @param cursor The underlying cursor to wrap.
     */
    public PhraseCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Phrase getPhrase()
    {
        UUID uuid = getUUID(PhraseDbSchema.PhraseTable.Cols.UUID);
        String title = getString(PhraseDbSchema.PhraseTable.Cols.Title);
        boolean isBookmarked = getBool(PhraseDbSchema.PhraseTable.Cols.Bookmarked);
        Phrase phrase = new Phrase(uuid, new ArrayList<PhraseTranslation>());
        phrase.setBookmarked(isBookmarked);
        phrase.setTitle(title);
        return phrase;
    }

    public PhraseTranslation getTranslation()
    {
        UUID uuid = getUUID(PhraseDbSchema.PhraseTranslationTable.Cols.UUID);
        UUID phraseUUID = getUUID(PhraseDbSchema.PhraseTranslationTable.Cols.Phrase_UUID);
        String title = getString(PhraseDbSchema.PhraseTranslationTable.Cols.Title);
        String phonetic = getString(PhraseDbSchema.PhraseTranslationTable.Cols.Phonetic);
        String languageTitle = getString(PhraseDbSchema.PhraseTranslationTable.Cols.LanguageTitle);
        String tags = getString(PhraseDbSchema.PhraseTranslationTable.Cols.Tags);
        String notes = getString(PhraseDbSchema.PhraseTranslationTable.Cols.Notes);
        PhraseTranslation translation = new PhraseTranslation(uuid, phraseUUID);
        translation.setTitle(title);
        translation.setPhonetic(phonetic);
        translation.setLanguageTitle(languageTitle);
        translation.setTags(tags);
        translation.setNotes(notes);
        return translation;
    }

    protected UUID getUUID(String colName)
    {
       return UUID.fromString(getString(getColumnIndex(colName)));
    }

    protected String getString(String colName)
    {
        return getString(getColumnIndex(colName));
    }

    protected boolean getBool(String colName)
    {
        return getInt(getColumnIndex(colName)) != 0;
    }

}
