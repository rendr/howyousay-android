package io.rendr.howyousay.core.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.rendr.howyousay.core.models.Phrase;
import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 12/31/15.
 * Copyright Rendr LLC All Rights Reserved
 */
public class PhraseJSONWrapper extends JSONObject {

    public PhraseJSONWrapper(Phrase phrase) throws JSONException{
        this.put("title", phrase.getTitle());
        JSONArray translations = new JSONArray();
        this.put("translations", translations);
        java.util.List<PhraseTranslation> translations1 = phrase.getTranslations();
        for (int i = 0; i < translations1.size(); i++) {
            PhraseTranslation translation = translations1.get(i);
            translations.put(i, new PhraseTranslationJSONWrapper(translation));
        }


    }


}
