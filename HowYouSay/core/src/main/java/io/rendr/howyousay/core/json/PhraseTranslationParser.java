package io.rendr.howyousay.core.json;

import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import io.rendr.howyousay.core.models.PhraseTranslation;

/**
 * Created by ben on 12/22/15.
 * Copyright Rendr LLC All Rights Reserved
 */
@EBean
public class PhraseTranslationParser {


    public PhraseTranslation parse(JSONObject jsonObject, UUID phraseID) throws JSONException
    {
        PhraseTranslation translation = new PhraseTranslation(UUID.randomUUID(), phraseID);
        translation.setNotes(jsonObject.getString("notes"));
        translation.setTitle(jsonObject.getString("content"));
        translation.setPhonetic(jsonObject.getString("phonetic"));
        translation.setLanguageTitle(jsonObject.getString("language"));

        return translation;
    }
}
